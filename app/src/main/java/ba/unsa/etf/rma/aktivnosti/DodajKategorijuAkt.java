package ba.unsa.etf.rma.aktivnosti;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.PodService;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.connected;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback {
    private EditText nazivKategorije;
    private EditText idIkone;
    private Button dodajIkonu;
    private Button dodajKategoriju;
    public static Kategorija novaKategorija;
    IconDialog iconDialog = new IconDialog();
    private Icon[] selectedIcons;
    boolean postojiKategorija = false;
    ArrayList<Pitanje> pitanjaUKvizu = new ArrayList<>();
    ArrayList<String> naziviSvihKategorija = new ArrayList<>();
    ArrayList<Pitanje> mogucaPitanja = new ArrayList<>();
    String nazivKviza = "";

    public static class DodajKategorijuTask extends AsyncTask<String, Integer, Void> {
        AsyncResponse delegat = null;
        Context context;

        public DodajKategorijuTask (AsyncResponse delegat, Context context) {
            this.delegat = delegat;
            this.context = context;
        }
        @Override
        protected Void doInBackground(String... strings) {
            kategorijeBaza.clear();
            InputStream is = context.getResources().openRawResource(R.raw.secret);
            GoogleCredential credentials = null;
            try {
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                //ucitavanje kategorija iz baze
                String url2 = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Kategorije?access_token=";
                HttpURLConnection conn2 = null;
                try {
                    URL urlObj = new URL(url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                    conn2 = (HttpURLConnection) urlObj.openConnection();
                    conn2.setRequestProperty("Authorization", "Bearer"+TOKEN);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                InputStream inputStream2 = new BufferedInputStream(conn2.getInputStream());
                String rezultat2 = PodService.convertStreamToString(inputStream2);
                JSONObject jo2 = new JSONObject(rezultat2);
                String naziv2 = "";
                JSONObject fields2 = null;
                JSONArray joKategorije = null;
                if (jo2.has("documents")) {
                    joKategorije = jo2.getJSONArray("documents");
                    for (int i = 0; i < joKategorije.length(); i++) {
                        JSONObject kat = joKategorije.getJSONObject(i);
                        if (kat.has("fields")) {
                            fields2 = kat.getJSONObject("fields");
                            JSONObject ime;
                            if (fields2.has("naziv")) {
                                ime = fields2.getJSONObject("naziv");
                                naziv2 = ime.getString("stringValue");
                                Kategorija nova = new Kategorija();
                                nova.setNaziv(naziv2);
                                JSONObject ikona = fields2.getJSONObject("idIkonice");
                                int id = ikona.getInt("integerValue");
                                nova.setId(String.valueOf(id));
                                kategorijeBaza.add(nova);
                            }
                        }
                    }
                }
                boolean postojiKategorijaUBazi = false;
                if (kategorijeBaza != null && kategorijeBaza.size() != 0) {
                    for (Kategorija k : kategorijeBaza) {
                        if (k != null && k.getNaziv().equals(novaKategorija.getNaziv())) {
                            postojiKategorijaUBazi = true;
                        }
                    }
                }
                if (!postojiKategorijaUBazi) { // dodavanje kategorije ako ne postoji u bazi
                    String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Kategorije?documentId=" + novaKategorija.getNaziv() + "&access_token=";
                    HttpURLConnection conn = null;
                    try {
                        URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                        conn = (HttpURLConnection) urlObj.openConnection();
                         conn.setDoOutput(true);
                         conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("Accept", "application/json");
                        //conn.setRequestProperty("Authorization", "Bearer" + TOKEN);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                        String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + novaKategorija.getNaziv() + "\"}, \"idIkonice\": {\"integerValue\":\"";
                        if (novaKategorija != null && novaKategorija.getId() != null) {
                            int idKategorije = Integer.valueOf(novaKategorija.getId());
                            dokument = dokument + idKategorije;
                        } else {
                            dokument = dokument + "null";
                        }
                        dokument = dokument + "\"}}}";

                        try (OutputStream os = conn.getOutputStream()) {
                            byte[] input = dokument.getBytes(StandardCharsets.UTF_8);
                            os.write(input, 0, input.length);
                        }
                        int code = conn.getResponseCode();
                        InputStream odgovor = conn.getInputStream();
                        try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, StandardCharsets.UTF_8))) {
                            StringBuilder response = new StringBuilder();
                            String responseLine = null;
                            while ((responseLine = br.readLine()) != null) {
                                response.append(responseLine.trim());
                            }
                            Log.d("ODGOVOR", response.toString());
                        }
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context).setMessage("Kategorija već postoji!");
                    alert.show();
                }
            } catch(IOException e){
                e.printStackTrace();
            } catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kategoriju);
        novaKategorija = new Kategorija();

        nazivKategorije = findViewById(R.id.etNaziv);
        idIkone = findViewById(R.id.etIkona);
        idIkone.setFocusable(false);
        dodajIkonu = findViewById(R.id.btnDodajIkonu);
        dodajKategoriju = findViewById(R.id.btnDodajKategoriju);

        naziviSvihKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");
        pitanjaUKvizu = getIntent().getParcelableArrayListExtra("pitanjaUKvizu");
       // naziviPitanja = getIntent().getStringArrayListExtra("naziviPitanja");
        nazivKviza = getIntent().getStringExtra("nazivKviza");
        mogucaPitanja = getIntent().getParcelableArrayListExtra("mogucaPitanja");

        dodajIkonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iconDialog.setMaxSelection(1, true);
                iconDialog.setSelectedIcons(selectedIcons);
                iconDialog.show(getSupportFragmentManager(), "icon_dialog");
            }
        });

        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connected = spojenoNaMrezu();
                if (connected) {
                    final String naziv = nazivKategorije.getText().toString();
                    if (!naziv.equals("")) {
                        postojiKategorija = false;
                        for (Kategorija k : kategorije) {
                            if (k != null && k.getNaziv().equals(naziv)) {
                                postojiKategorija = true;
                                break;
                            }
                        }
                    }
                    if (!postojiKategorija && !idIkone.getText().toString().equals("")) {
                        novaKategorija.setNaziv(naziv);
                        if (trenutniKviz != null) {
                            trenutniKviz.setKategorija(novaKategorija);
                        }
                        naziviSvihKategorija.add(naziv);
                        kategorije.add(novaKategorija);

                        // dodavanje kategorije u bazu
                        new DodajKategorijuTask(new AsyncResponse() {
                            @Override
                            public void processFinish(String output) {
                                kategorije.clear();
                                kategorije.addAll(kategorijeBaza);
                                naziviSvihKategorija.clear();
                                naziviSvihKategorija.add("Sve kategorije");
                                naziviSvihKategorija.add("Dodaj kategoriju");
                                if (kategorijeBaza != null && kategorijeBaza.size() != 0) {
                                    for (Kategorija k : kategorijeBaza) {
                                        if (k != null)
                                            naziviSvihKategorija.add(k.getNaziv());
                                    }
                                }
                            }
                        }, DodajKategorijuAkt.this).execute(" ");

                        Intent returnIntent = new Intent(DodajKategorijuAkt.this, DodajKvizAkt.class);
                        returnIntent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                        returnIntent.putExtra("nazivKviza", nazivKviza);
                        returnIntent.putExtra("novaKategorija", novaKategorija);
                        returnIntent.putExtra("pitanjaUKvizu", pitanjaUKvizu);
                        returnIntent.putExtra("mogucaPitanja", mogucaPitanja);
                        DodajKategorijuAkt.this.startActivity(returnIntent);
                    } else {
                        nazivKategorije.setBackgroundColor(Color.RED);
                        AlertDialog.Builder alert = new AlertDialog.Builder(DodajKategorijuAkt.this).setMessage("Kategorija već postoji!").setPositiveButton("OK", null);
                        alert.show();
                    }
                } else Toast.makeText(DodajKategorijuAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean spojenoNaMrezu() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
            return true;
        return false;
    }

    @Override
    public void onIconDialogIconsSelected (Icon[] icons) {
        selectedIcons = icons;
        idIkone.setText(String.valueOf(selectedIcons[0].getId()));
        if (selectedIcons != null) {
            novaKategorija.setId(String.valueOf(selectedIcons[0].getId()));
        }
    }
}
