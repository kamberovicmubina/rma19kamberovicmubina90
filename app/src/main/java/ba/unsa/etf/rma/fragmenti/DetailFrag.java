package ba.unsa.etf.rma.fragmenti;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.klase.CustomAdapter;
import ba.unsa.etf.rma.klase.CustomAdapterZaSiriEkran;
import ba.unsa.etf.rma.klase.Kviz;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.sviKvizovi;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.fragmenti.ListaFrag.pozicijaOdabraneKategorije;

public class DetailFrag extends Fragment {
    GridView gridKvizovi;
    private ArrayList<String> naziviSvihKategorija = new ArrayList<>();
    private ArrayList<Kviz> prikazaniKvizovi = new ArrayList<>();
    String nazivKviza = "";
    String nazivKategorije = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.fragment_detail_frag, container, false);
        gridKvizovi = iv.findViewById(R.id.gridKvizovi);
        return iv;
    }


    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prikazaniKvizovi = new ArrayList<>();
        naziviSvihKategorija = getActivity().getIntent().getStringArrayListExtra("naziviSvihKategorija");
        nazivKviza = getActivity().getIntent().getStringExtra("nazivKviza");
        nazivKategorije = getActivity().getIntent().getStringExtra("nazivKategorije");

        if (getArguments() != null && getArguments().containsKey("naziviSvihKategorija")) {
            naziviSvihKategorija = getArguments().getStringArrayList("naziviSvihKategorija");
        }
        // footer
        Kviz k = new Kviz();
        k.setNaziv("Dodaj kviz");
        prikazaniKvizovi.add(k);

        setListData(pozicijaOdabraneKategorije);

        gridKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (!prikazaniKvizovi.get(position).getNaziv().equals("Dodaj kviz")) {
                    trenutniKviz = prikazaniKvizovi.get(position);
                } else trenutniKviz = null;
                Intent intent = new Intent(getContext(), DodajKvizAkt.class);
                intent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                if (trenutniKviz != null && trenutniKviz.getKategorija() != null) {
                    intent.putExtra("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
                    intent.putExtra("nazivKviza", trenutniKviz.getNaziv());
                }
                startActivity(intent);
                return true;
            }
        });

        gridKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0 && position < prikazaniKvizovi.size()) {
                    if (!prikazaniKvizovi.get(position).getNaziv().equals("Dodaj kviz")) {
                        trenutniKviz = prikazaniKvizovi.get(position);
                        Intent intent = new Intent(getContext(), IgrajKvizAkt.class);
                        intent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                        if (trenutniKviz != null && trenutniKviz.getKategorija() != null) {
                            intent.putExtra("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
                            intent.putExtra("nazivKviza", trenutniKviz.getNaziv());
                        }
                        startActivity(intent);
                    }
                }
            }
        });

    }


    public void setListData (int idKategorije) {
        if (idKategorije == 0) {
            //prikazaniKvizovi.clear();
            for (Kviz k : sviKvizovi) {
                prikazaniKvizovi.add(k);
            }
        } else {
            //prikazaniKvizovi.clear();
            String pomocna = naziviSvihKategorija.get(idKategorije);
            for (Kviz k : sviKvizovi) {
                if (k.getKategorija() == null && pomocna.equals("Sve kategorije")) {
                    prikazaniKvizovi.add(k);
                } else if (k.getKategorija() != null && pomocna.equals(k.getKategorija().getNaziv())) {
                    prikazaniKvizovi.add(k);
                }
            }
        }
        CustomAdapterZaSiriEkran customAdapter = new CustomAdapterZaSiriEkran(getContext(), prikazaniKvizovi);
        gridKvizovi.setAdapter(customAdapter);
    }
}
