package ba.unsa.etf.rma.helper;

public class KategorijaDBOpenHelper {
    public static final String DATABASE_TABLE_KATEGORIJE = "kategorije";
    public static final String KATEGORIJA_ID = "_id";
    public static final String KATEGORIJA_NAZIV = "nazivKategorije";
    public static final String ID_IKONE = "idIkonice";

    public static final String CREATE_KATEGORIJE = "create table " + DATABASE_TABLE_KATEGORIJE +
            "(" + KATEGORIJA_ID + " integer primary key autoincrement, " + KATEGORIJA_NAZIV + " TEXT, " +
            ID_IKONE + " text);";



}
