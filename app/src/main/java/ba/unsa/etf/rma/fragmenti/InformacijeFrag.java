package ba.unsa.etf.rma.fragmenti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.task.DodajURanglistuTask;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.fragmenti.PitanjeFrag.brojacPitanja;
import static ba.unsa.etf.rma.fragmenti.PitanjeFrag.prikazanoPitanje;

public class InformacijeFrag extends Fragment {
    String nazivKviza = "";
    TextView naziv;
    TextView brojTacnih;
    TextView brojPreostalih;
    TextView procenatTacnih;
    Button dugmeKraj;
    ArrayList<String> naziviSvihKategorija;
    String nazivKategorije = "";
    public static int brojTacnihOdg = 0;
    public static int brojPreostalihOdg = 0;
    public static double procenatTacnihOdg = 0.;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        procenatTacnihOdg = 0.;
        nazivKviza = "";
        View iv= inflater.inflate(R.layout.fragment_informacije, container, false);

        naziv = iv.findViewById(R.id.infNazivKviza);
        brojTacnih = iv.findViewById(R.id.infBrojTacnihPitanja);
        brojPreostalih = iv.findViewById(R.id.infBrojPreostalihPitanja);
        procenatTacnih = iv.findViewById(R.id.infProcenatTacni);
        dugmeKraj = iv.findViewById(R.id.btnKraj);

        return iv;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        naziviSvihKategorija = getActivity().getIntent().getStringArrayListExtra("naziviSvihKategorija");
        nazivKviza = getActivity().getIntent().getStringExtra("nazivKviza");
        nazivKategorije = getActivity().getIntent().getStringExtra("nazivKategorije");

        if (getArguments() != null && getArguments().containsKey("pritisnutiOdgovor")) {
            int odg = getArguments().getInt("pritisnutiOdgovor");
            naziv.setText(trenutniKviz.getNaziv());
            brojTacnih.setText(String.valueOf(brojTacnihOdg));
            brojPreostalih.setText(String.valueOf(brojPreostalihOdg));
            int brojOdgovorenih = trenutniKviz.getPitanja().size() - brojPreostalihOdg;
            if (brojOdgovorenih != 0)
                procenatTacnihOdg = ((double) brojTacnihOdg/brojOdgovorenih)*100;
            String procenat = procenatTacnihOdg + "%";
            procenatTacnih.setText(procenat);
            if (brojacPitanja == 0) {
                brojTacnihOdg = 0;
                brojPreostalihOdg = 0;
            }

        } else {
            int brojOdgovorenih = 0;
            if (trenutniKviz.getPitanja() != null) {
                brojPreostalihOdg = trenutniKviz.getPitanja().size();
                brojOdgovorenih = trenutniKviz.getPitanja().size() - brojPreostalihOdg;
            } else brojPreostalihOdg = 0;
            naziv.setText(trenutniKviz.getNaziv());
            brojTacnih.setText(String.valueOf(brojTacnihOdg));
            brojPreostalih.setText(String.valueOf(brojPreostalihOdg));
            if (brojOdgovorenih != 0)
                procenatTacnihOdg = ((double) brojTacnihOdg/brojOdgovorenih)*100;
            String procenat = procenatTacnihOdg + "%";
            procenatTacnih.setText(procenat);
        }

        dugmeKraj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), KvizoviAkt.class);
                intent.putExtra("nazivKviza", nazivKviza);
                intent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                intent.putExtra("nazivKategorije", nazivKategorije);
                startActivity(intent);

            }
        });

    }

}
