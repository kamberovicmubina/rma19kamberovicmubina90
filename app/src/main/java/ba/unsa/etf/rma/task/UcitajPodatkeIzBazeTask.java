package ba.unsa.etf.rma.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.PodService;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kvizoviBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaBaza;

public class UcitajPodatkeIzBazeTask extends AsyncTask<String, Void, String> {
    AsyncResponse delegat = null;
    Context context = null;

    public UcitajPodatkeIzBazeTask (AsyncResponse delegat, Context context) {
        this.delegat = delegat;
        this.context = context;
    }
    @Override
    protected String doInBackground(String... strings) {
        kategorijeBaza.clear();
        pitanjaBaza.clear();
        kvizoviBaza.clear();
        InputStream is = context.getResources().openRawResource(R.raw.secret);
        GoogleCredential credentials = null;
        try {
            credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKEN", TOKEN);

            //ucitavanje kategorija iz baze
            String url2 = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Kategorije?access_token=";
            HttpURLConnection conn2 = null;
            try {
                URL urlObj = new URL(url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                conn2 = (HttpURLConnection) urlObj.openConnection();
                conn2.setRequestProperty("Authorization", "Bearer"+TOKEN);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            InputStream inputStream2 = new BufferedInputStream(conn2.getInputStream());
            String rezultat2 = PodService.convertStreamToString(inputStream2);
            JSONObject jo2 = new JSONObject(rezultat2);
            String naziv2 = "";
            JSONObject fields2 = null;
            JSONArray joKategorije = null;
            if (jo2.has("documents")) {
                joKategorije = jo2.getJSONArray("documents");
                for (int i = 0; i < joKategorije.length(); i++) {
                    JSONObject kat = joKategorije.getJSONObject(i);
                    if (kat.has("fields")) {
                        fields2 = kat.getJSONObject("fields");
                        JSONObject ime;
                        if (fields2.has("naziv")) {
                            ime = fields2.getJSONObject("naziv");
                            naziv2 = ime.getString("stringValue");
                            Kategorija nova = new Kategorija();
                            nova.setNaziv(naziv2);
                            JSONObject ikona = fields2.getJSONObject("idIkonice");
                            int id = ikona.getInt("integerValue");
                            nova.setId(String.valueOf(id));
                            kategorijeBaza.add(nova);
                        }
                    }
                }
            }
            //ucitavanje pitanja iz baze
            String url3 = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Pitanja?access_token=";
            HttpURLConnection conn3 = null;
            try {
                URL urlObj = new URL(url3 + URLEncoder.encode(TOKEN, "UTF-8"));
                conn3 = (HttpURLConnection) urlObj.openConnection();
                conn3.setRequestProperty("Authorization", "Bearer"+TOKEN);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            InputStream inputStream3 = new BufferedInputStream(conn3.getInputStream());
            String rezultat3 = PodService.convertStreamToString(inputStream3);
            JSONObject jo3 = new JSONObject(rezultat3);
            String naziv3 = "";
            JSONObject fields3 = null;
            JSONArray joPitanja = null;
            if (jo3.has("documents")) {
                joPitanja = jo3.getJSONArray("documents");
                for (int i = 0; i < joPitanja.length(); i++) {
                    JSONObject pitanjaJSONObject = joPitanja.getJSONObject(i);
                    if (pitanjaJSONObject.has("fields")) {
                        fields3 = pitanjaJSONObject.getJSONObject("fields");
                        JSONObject ime;
                        if (fields3.has("naziv")) {
                            ime = fields3.getJSONObject("naziv");
                            naziv3 = ime.getString("stringValue");
                            Pitanje novo = new Pitanje();
                            novo.setNaziv(naziv3);
                            novo.setTekstPitanja(naziv3);
                            ArrayList<String> odg = new ArrayList<>();
                            if (fields3.has("odgovori")) {
                                JSONObject od = fields3.getJSONObject("odgovori");
                                JSONObject array = od.getJSONObject("arrayValue");
                                JSONArray values = array.getJSONArray("values");
                                for (int j = 0; j < values.length(); j++) {
                                    JSONObject odgovor = values.getJSONObject(j);
                                    String tekst = odgovor.getString("stringValue");
                                    odg.add(tekst);
                                }
                            }
                            novo.setOdgovori(odg);
                            if (fields3.has("indexTacnog")) {
                                JSONObject indeks = fields3.getJSONObject("indexTacnog");
                                int ind = indeks.getInt("integerValue");
                                novo.setTacan(odg.get(ind));
                            }
                            pitanjaBaza.add(novo);
                        }
                    }
                }
            }
            //ucitavanje kvizova iz baze
            HttpURLConnection conn = null;
            String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Kvizovi?access_token=";
            try {
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer"+TOKEN);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            InputStream inputStream = new BufferedInputStream(conn.getInputStream());
            String rezultat = PodService.convertStreamToString(inputStream);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray joKvizovi = null;
            if (jo.has("documents")) {
                joKvizovi = jo.getJSONArray("documents");
                String naziv = "";
                JSONObject fields = null;
                for (int i = 0; i < joKvizovi.length(); i++) {
                    JSONObject kv = joKvizovi.getJSONObject(i);
                    if (kv.has("fields")) {
                        fields = kv.getJSONObject("fields");
                        JSONObject ime;
                        if (fields.has("naziv")) {
                            ime = fields.getJSONObject("naziv");
                            naziv = ime.getString("stringValue");
                        }
                        Kviz novi = new Kviz();
                        novi.setNaziv(naziv);
                        novi.setId(naziv);
                        JSONObject kategorija = fields.getJSONObject("idKategorije");
                        String idKategorije = kategorija.getString("stringValue");
                        for (Kategorija k : kategorijeBaza) {
                            if (k != null && k.getNaziv().equals(idKategorije)) {
                                novi.setKategorija(k);
                                break;
                            }
                        }
                        ArrayList<String> idPitanja = new ArrayList<>();
                        JSONObject pitanja = fields.getJSONObject("pitanja");
                        JSONObject arrayValue = pitanja.getJSONObject("arrayValue");
                        JSONArray pitanjaNiz = null;
                        if (arrayValue.has("values")) {
                            pitanjaNiz = arrayValue.getJSONArray("values");
                            for (int j = 0; j < pitanjaNiz.length(); j++) {
                                JSONObject pitanje = pitanjaNiz.getJSONObject(j);
                                String nazivPitanja = pitanje.getString("stringValue");
                                idPitanja.add(nazivPitanja);
                            }
                            // sada trazimo objekat Pitanje koji odgovara nazivu pitanja koje je upravo dodano u listu idPitanja
                            ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                            if (pitanjaBaza != null && pitanjaBaza.size() != 0) {
                                pitanjaKviza.clear();
                                for (String s : idPitanja) {
                                    for (Pitanje p : pitanjaBaza) {
                                        if (s.equals(p.getNaziv())) {
                                            pitanjaKviza.add(p);
                                        }
                                    }
                                }
                            }
                            novi.setPitanja(pitanjaKviza);
                        }
                        kvizoviBaza.add(novi);
                        // prikazaniKvizovi.add(novi);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }
    @Override
    protected void onPostExecute (String res) {
        delegat.processFinish(res);
    }
}
