package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.PodService;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.connected;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;

public class DodajPitanjeAkt extends AppCompatActivity {
    String tacanOdgovor = "";
    //ArrayList<String> naziviPitanja = new ArrayList<>();
    ArrayList<String> naziviSvihKategorija = new ArrayList<>();
    ArrayList<Pitanje> pitanjaUKvizu = new ArrayList<>();
    ArrayList<Pitanje> mogucaPitanja = new ArrayList<>();
    String nazivKviza;
    String nazivKategorije;
    private int pozicijaTacnog;
    public static Pitanje novoPitanje = new Pitanje();
    ArrayList<String> sviOdgovori = new ArrayList<>();

    public static class DodajPitanjeTask extends AsyncTask<String, Integer, Void> {
        AsyncResponse delegat = null;
        Context context;

        public DodajPitanjeTask (AsyncResponse delegat, Context context) {
            this.delegat = delegat;
            this.context = context;
        }
        @Override
        protected Void doInBackground(String... strings) {
            pitanjaBaza.clear();
            InputStream is = context.getResources().openRawResource(R.raw.secret);
            GoogleCredential credentials = null;
            try {
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                //Log.d("TOKEN", TOKEN);
                //ucitavanje pitanja iz baze
                String url2 = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Pitanja?access_token=";
                HttpURLConnection conn3 = null;
                try {
                    URL urlObj = new URL(url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                    conn3 = (HttpURLConnection) urlObj.openConnection();
                    conn3.setRequestProperty("Authorization", "Bearer"+TOKEN);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                InputStream inputStream3 = new BufferedInputStream(conn3.getInputStream());
                String rezultat3 = PodService.convertStreamToString(inputStream3);
                JSONObject jo3 = new JSONObject(rezultat3);
                String naziv3;
                JSONObject fields3;
                JSONArray joPitanja = null;
                if (jo3.has("documents")) {
                    joPitanja = jo3.getJSONArray("documents");
                    for (int i = 0; i < joPitanja.length(); i++) {
                        JSONObject pitanjaJSONObject = joPitanja.getJSONObject(i);
                        if (pitanjaJSONObject.has("fields")) {
                            fields3 = pitanjaJSONObject.getJSONObject("fields");
                            JSONObject ime;
                            if (fields3.has("naziv")) {
                                ime = fields3.getJSONObject("naziv");
                                naziv3 = ime.getString("stringValue");
                                Pitanje novo = new Pitanje();
                                novo.setNaziv(naziv3);
                                novo.setTekstPitanja(naziv3);
                                ArrayList<String> odg = new ArrayList<>();
                                if (fields3.has("odgovori")) {
                                    JSONObject od = fields3.getJSONObject("odgovori");
                                    JSONObject array = od.getJSONObject("arrayValue");
                                    JSONArray values = array.getJSONArray("values");
                                    for (int j = 0; j < values.length(); j++) {
                                        JSONObject odgovor = values.getJSONObject(j);
                                        String tekst = odgovor.getString("stringValue");
                                        odg.add(tekst);
                                    }
                                }
                                novo.setOdgovori(odg);
                                if (fields3.has("indexTacnog")) {
                                    JSONObject indeks = fields3.getJSONObject("indexTacnog");
                                    int ind = indeks.getInt("integerValue");
                                    novo.setTacan(odg.get(ind));
                                }
                                pitanjaBaza.add(novo);
                            }
                        }
                    }
                }
                boolean postojiPitanjeUBazi = false;
                if (pitanjaBaza != null && pitanjaBaza.size() != 0) {
                    for (Pitanje p : pitanjaBaza) {
                        if (p.getNaziv().equals(novoPitanje.getNaziv())) {
                            postojiPitanjeUBazi = true;
                            break;
                        }
                    }
                }

                Log.d("PITANJEEE \"" , postojiPitanjeUBazi + "\"");
                if (!postojiPitanjeUBazi) { // ubacuje u bazu ako ne postoji pitanje
                    String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Pitanja?documentId=" + novoPitanje.getNaziv() + "&access_token=";
                    HttpURLConnection conn = null;
                    try {
                        URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                        conn = (HttpURLConnection) urlObj.openConnection();
                        conn.setDoOutput(true);
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("Accept", "application/json");
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    int indeks = novoPitanje.getOdgovori().indexOf(novoPitanje.getTacan());
                    String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + novoPitanje.getNaziv() + "\"}, \"indexTacnog\": {\"integerValue\":\"" + indeks + "\"}" +
                            ", \"odgovori\": {\"arrayValue\": {\"values\": [";
                    for (int i = 0; i < novoPitanje.getOdgovori().size(); i++) {
                        if (i != novoPitanje.getOdgovori().size() - 1) {
                            dokument = dokument + "{\"stringValue\":\"" + novoPitanje.getOdgovori().get(i) + "\"}, ";
                        } else {
                            dokument = dokument + "{\"stringValue\":\"" + novoPitanje.getOdgovori().get(i) + "\"}";
                        }
                    }
                    dokument = dokument + "]}}}}";


                    try (OutputStream os = conn.getOutputStream()) {
                        byte[] input = dokument.getBytes(StandardCharsets.UTF_8);
                        os.write(input, 0, input.length);
                    }
                    int code = conn.getResponseCode();
                    InputStream odgovor = conn.getInputStream();
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, StandardCharsets.UTF_8))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        Log.d("ODGOVOR", response.toString());
                    }
                } else {
                    Log.d("GRESKA", "postoji pitanje");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_pitanje);
        Resources res = getResources();
        naziviSvihKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");
        //kategorija = (Kategorija) getIntent().getSerializableExtra("novaKategorija");
        nazivKategorije = getIntent().getStringExtra("nazivKategorije");
        nazivKviza = getIntent().getStringExtra("nazivKviza");
        pitanjaUKvizu = getIntent().getParcelableArrayListExtra("pitanjaUKvizu");
        mogucaPitanja = getIntent().getParcelableArrayListExtra("mogucaPitanja");

        final EditText nazivPitanja = findViewById(R.id.etNaziv);
        final EditText odgovorPitanja = findViewById(R.id.etOdgovor);
        final ListView odgovori = findViewById(R.id.lvOdgovori);
        Button dodajOdgovorBtn = findViewById(R.id.btnDodajOdgovor);
        final Button dodajTacanOdgovor = findViewById(R.id.btnDodajTacan);
        Button dodajPitanje = findViewById(R.id.btnDodajPitanje);

        pozicijaTacnog = -1;
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, sviOdgovori) {
            @Override
            public View getView (int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == pozicijaTacnog) {
                    v.setBackgroundColor(Color. GREEN);
                }
                return v;
            }
        };

        dodajOdgovorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String noviOdgovor = odgovorPitanja.getText().toString();
                boolean postojiOdgovor = false;
                if (!noviOdgovor.equals("")) {
                    for (String s : sviOdgovori) {
                        if (s.equals(noviOdgovor)) {
                            postojiOdgovor = true;
                        }
                    }
                    if (!postojiOdgovor) {
                        sviOdgovori.add(noviOdgovor);
                        //ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, sviOdgovori);
                        odgovori.setAdapter(adapter);
                    }
                }

            }
        });

        dodajTacanOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tacanOdgovor = odgovorPitanja.getText().toString();
                dodajTacanOdgovor.setClickable(false);
                boolean postojiOdgovor = false;
                if (!tacanOdgovor.equals("")) {
                    for (String s : sviOdgovori) { // provjera da li odgovor vec postoji
                        if (s.equals(tacanOdgovor)) {
                            postojiOdgovor = true;
                            //promijeniti samo boju elementa
                        }
                    }
                    if (!postojiOdgovor) {
                        sviOdgovori.add(tacanOdgovor);
                        //ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, sviOdgovori);
                        odgovori.setAdapter(adapter);
                        pozicijaTacnog = adapter.getPosition(tacanOdgovor);
                        /*adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, sviOdgovori) {
                            @Override
                            public View getView (int position, View convertView, ViewGroup parent) {
                                View v = super.getView(position, convertView, parent);
                                if (position == pozicija) {
                                    v.setBackgroundColor(Color. GREEN);
                                }
                                return v;
                            }
                        };*/
                        odgovori.setAdapter(adapter);
                    }
                }
            }
        });

        dodajPitanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connected = spojenoNaMrezu();
                if (connected) {
                    if (!tacanOdgovor.equals("")) {
                        novoPitanje.setOdgovori(sviOdgovori);
                        novoPitanje.setTacan(tacanOdgovor);
                        novoPitanje.setNaziv(nazivPitanja.getText().toString());
                        boolean postojiPitanje = false;
                        for (Pitanje p : pitanjaUKvizu) {
                            if (p != null && p.getNaziv().equals(novoPitanje.getNaziv())) {
                                postojiPitanje = true;
                                break;
                            }
                        }
                        if (!postojiPitanje) {
                            pitanjaUKvizu.add(novoPitanje);

                            // dodavanje pitanja u bazu
                            new DodajPitanjeTask(new AsyncResponse() {
                                @Override
                                public void processFinish(String output) {

                                }
                            }, DodajPitanjeAkt.this).execute(" ");
                        }

                        if (trenutniKviz != null) trenutniKviz.setPitanja(pitanjaUKvizu);
                        Intent intent = new Intent(DodajPitanjeAkt.this, DodajKvizAkt.class);
                        intent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                        intent.putExtra("nazivKviza", nazivKviza);
                        intent.putExtra("nazivKategorije", nazivKategorije);
                        intent.putExtra("pitanjaUKvizu", pitanjaUKvizu);
                        intent.putExtra("mogucaPitanja", mogucaPitanja);
                        DodajPitanjeAkt.this.startActivity(intent);
                    } else {
                        odgovorPitanja.setBackgroundColor(Color.RED);
                    }
                } else Toast.makeText(DodajPitanjeAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean spojenoNaMrezu() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
            return true;
        return false;
    }
}
