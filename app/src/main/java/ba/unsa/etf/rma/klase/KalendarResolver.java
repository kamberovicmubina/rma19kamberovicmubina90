package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.database.Cursor;
import android.provider.CalendarContract;

import java.util.HashMap;
import java.util.Map;

public class KalendarResolver {
    Context context;

    public KalendarResolver(Context context) {
        this.context = context;
    }

    public Map<String, String> uzmiPodatke() {
        Map<String, String> mapa = new HashMap<>();
        Cursor cursor = null;
        String[] ulaz = new String[] {CalendarContract.Events.DTSTART, CalendarContract.Events.TITLE};

        try {
           cursor = context.getContentResolver().query(CalendarContract.Events.CONTENT_URI, ulaz, null, null, null);
           String poc, naziv;
           while (cursor.moveToNext()) {
               poc = cursor.getString(0);
               naziv = cursor.getString(1);
               mapa.put(poc, naziv);
           }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return mapa;
    }

}
