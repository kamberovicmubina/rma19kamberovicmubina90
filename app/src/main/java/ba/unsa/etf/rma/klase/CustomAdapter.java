package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.maltaisn.icondialog.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.maltaisn.icondialog.IconHelper;
import java.util.List;
import ba.unsa.etf.rma.R;

public class CustomAdapter extends ArrayAdapter<Kviz> {

    public CustomAdapter (Context context, List<Kviz> d) {
        super(context, 0, d);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Kviz kviz = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.tabitem, parent, false);
        }

        TextView nazivKviza = convertView.findViewById(R.id.text);
        nazivKviza.setText(kviz.getNaziv());

        if (kviz.getKategorija() != null && kviz.getKategorija().getId() != null) {
            final ImageView slikaKategorije = convertView.findViewById(R.id.image);
            Icon ikona;
            int id = Integer.parseInt(kviz.getKategorija().getId());
            ikona = IconHelper.getInstance(parent.getContext()).getIcon(id);
            if (ikona != null) {
                Drawable drawable = ikona.getDrawable(parent.getContext());
                slikaKategorije.setImageDrawable(drawable);
            }
        }
        return convertView;
    }
}