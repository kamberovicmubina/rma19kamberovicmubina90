package ba.unsa.etf.rma.fragmenti;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kviz;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.sviKvizovi;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;

public class ListaFrag extends Fragment {
    ListView listaKategorija;
    public static int pozicijaOdabraneKategorije;
    private ArrayList<String> naziviSvihKategorija = new ArrayList<>();
    private ArrayList<Kviz> prikazaniKvizovi = new ArrayList<>();
    String nazivKviza = "";
    String nazivKategorije = "";
    onItemClick oic;

    public interface onItemClick {
        void onItemClicked(int pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View iv = inflater.inflate(R.layout.fragment_lista_frag, container, false);
        listaKategorija = iv.findViewById(R.id.listaKategorija);
        return iv;
    }


    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        naziviSvihKategorija = getActivity().getIntent().getStringArrayListExtra("naziviSvihKategorija");
        nazivKviza = getActivity().getIntent().getStringExtra("nazivKviza");
        nazivKategorije = getActivity().getIntent().getStringExtra("nazivKategorije");

        if (naziviSvihKategorija == null) { // inicijalni display, tek se pokrenula aplikacija
            kategorije = new ArrayList<>();
            sviKvizovi = new ArrayList<>();
            prikazaniKvizovi = new ArrayList<>();
            naziviSvihKategorija = new ArrayList<>();

            naziviSvihKategorija.add(0, "Sve kategorije"); // nema drugih kategorija za dodati na samom pocetku

        } else if (naziviSvihKategorija.size() >= 2) {
            naziviSvihKategorija.remove("Dodaj kategoriju");
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, naziviSvihKategorija);
        listaKategorija.setAdapter(adapter);

        try {
            oic = (ListaFrag.onItemClick) getActivity();
        } catch (ClassCastException e) {

        }


        listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pozicijaOdabraneKategorije = position;
                oic.onItemClicked(position);
            }
        });




    }

}
