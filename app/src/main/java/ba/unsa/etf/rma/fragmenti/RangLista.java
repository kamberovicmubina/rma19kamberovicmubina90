package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import ba.unsa.etf.rma.R;

public class RangLista extends Fragment {
    ListView rangListaView;
    ArrayAdapter<String> adapter;
    public static ArrayList<String> podaciOIgrama;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View iv= inflater.inflate(R.layout.fragment_ranglista, container, false);
        rangListaView = iv.findViewById(R.id.rangList);
        return iv;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (podaciOIgrama != null) {
            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, podaciOIgrama);
            rangListaView.setAdapter(adapter);
        }


    }


}
