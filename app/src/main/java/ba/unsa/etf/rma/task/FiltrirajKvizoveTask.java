package ba.unsa.etf.rma.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.filtriraniKvizovi;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaBaza;

public class FiltrirajKvizoveTask extends AsyncTask<String, Void, String> {
    AsyncResponse delegat = null;
    Context context = null;

    public FiltrirajKvizoveTask (AsyncResponse delegat, Context context) {
        this.delegat = delegat;
        this.context = context;
    }
    public FiltrirajKvizoveTask (Context context) {
        this.context = context;
    }
    @Override
    protected String doInBackground(String... strings) {
        filtriraniKvizovi.clear();
        //  prikazaniKvizovi.clear();
        InputStream is = context.getResources().openRawResource(R.raw.secret);
        GoogleCredential credentials = null;
        try {
            credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKEN", TOKEN);

            String upit = "{\n \"structuredQuery\": {\n" +
                    "\"where\": {\n \"fieldFilter\": {\n" +
                    "\"field\": {\"fieldPath\": \"idKategorije\"}, \n" +
                    "\"op\": \"EQUAL\",\n" +
                    "\"value\": {\"stringValue\": \"" + strings[0] + "\"}\n" +
                    "}\n" +
                    "},\n" +
                    "\"select\": {\"fields\": [{\"fieldPath\": \"idKategorije\"}, {\"fieldPath\": \"naziv\"}, {\"fieldPath\": \"pitanja\"}]}, \n" +
                    "\"from\": [{\"collectionId\" : \"Kvizovi\"}], \n" +
                    "\"limit\" : 1000\n" +
                    "}\n}";
            String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents:runQuery?access_token=";
            HttpURLConnection conn = null;
            URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
            conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = upit.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            StringBuilder response = null;
            try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, StandardCharsets.UTF_8))) {
                response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                Log.d("ODGOVOR UPITA: ", response.toString());
            }

            String rezultat = response.toString();
            rezultat = "{\"documents\": " + rezultat + "}";
            if (!rezultat.equals("{}")) {
                JSONObject joKvizovi = new JSONObject(rezultat);
                JSONArray sviKvizoviDokumenti = joKvizovi.getJSONArray("documents");
                for (int i = 0; i < sviKvizoviDokumenti.length(); i++) {
                    JSONObject dokument = sviKvizoviDokumenti.getJSONObject(i);
                    if (dokument.has("document")) {
                        JSONObject kv = dokument.getJSONObject("document");
                        String naziv = "";
                        JSONObject fields = null;
                        if (kv.has("fields")) {
                            fields = kv.getJSONObject("fields");
                            JSONObject ime;
                            if (fields.has("naziv")) {
                                ime = fields.getJSONObject("naziv");
                                naziv = ime.getString("stringValue");
                            }
                            Kviz novi = new Kviz();
                            novi.setNaziv(naziv);
                            novi.setId(naziv); // mijenja se id u sustini
                            JSONObject kategorija = fields.getJSONObject("idKategorije");
                            String idKategorije = kategorija.getString("stringValue");
                            for (Kategorija k : kategorije) {
                                if (k != null && k.getId().equals(idKategorije)) {
                                    novi.setKategorija(k);
                                    break;
                                }
                            }
                            ArrayList<String> idPitanja = new ArrayList<>();
                            JSONObject pitanja = fields.getJSONObject("pitanja");
                            JSONObject arrayValue = pitanja.getJSONObject("arrayValue");
                            JSONArray pitanjaNiz = null;
                            if (arrayValue.has("values")) {
                                pitanjaNiz = arrayValue.getJSONArray("values");
                                for (int k = 0; k < pitanjaNiz.length(); k++) {
                                    JSONObject pitanje = pitanjaNiz.getJSONObject(k);
                                    String nazivPitanja = pitanje.getString("stringValue");
                                    idPitanja.add(nazivPitanja);
                                }
                                ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                                if (pitanjaBaza != null && pitanjaBaza.size() != 0) {
                                    for (String s : idPitanja) {
                                        for (Pitanje p : pitanjaBaza) {
                                            if (s.equals(p.getNaziv())) {
                                                pitanjaKviza.add(p);
                                            }
                                        }
                                    }
                                }
                                novi.setPitanja(pitanjaKviza);
                            }
                            filtriraniKvizovi.add(novi);
                            //prikazaniKvizovi.add(novi);
                        }

                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute (String res) {
        delegat.processFinish(res);
    }
}
