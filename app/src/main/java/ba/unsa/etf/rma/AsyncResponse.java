package ba.unsa.etf.rma;

public interface AsyncResponse {
    void processFinish(String output);
}
