package ba.unsa.etf.rma.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.PodService;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.fragmenti.RangLista.podaciOIgrama;

public class UcitajRanglistuTask extends AsyncTask<String, Void, String> {
    Context context;
    AsyncResponse delegat;

    public UcitajRanglistuTask(Context context, AsyncResponse delegat) {
        this.context = context;
        this.delegat = delegat;
    }

    @Override
    protected String doInBackground(String... strings) {
        InputStream is = context.getResources().openRawResource(R.raw.secret);
        GoogleCredential credentials = null;
        try {
            credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            String TOKEN = credentials.getAccessToken();
            Log.d("TOKEN UCITAVANJE", TOKEN);
            String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Rangliste?access_token=";
            HttpURLConnection conn = null;
            try {
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer"+TOKEN);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            InputStream inputStream = new BufferedInputStream(conn.getInputStream());
            String rezultat = PodService.convertStreamToString(inputStream);
            JSONObject jo = new JSONObject(rezultat);
            String naziv;
            JSONObject fields;
            JSONArray joRanglista = null;
            podaciOIgrama.clear();
            if (jo.has("documents")) {
                joRanglista = jo.getJSONArray("documents");
                for (int i = 0; i < joRanglista.length(); i++) {
                    JSONObject jedanKviz = joRanglista.getJSONObject(i);
                    if (jedanKviz.has("fields")) {
                        fields = jedanKviz.getJSONObject("fields");
                        JSONObject nazivKvizaObjekat;
                        if (fields.has("nazivKviza")) {
                            nazivKvizaObjekat = fields.getJSONObject("nazivKviza");
                            String nazivKviza = nazivKvizaObjekat.getString("stringValue");
                            if (nazivKviza.equals(trenutniKviz.getId())) {
                                JSONObject lista;
                                if (fields.has("lista")) {
                                    lista = fields.getJSONObject("lista");
                                    JSONObject mapValue;
                                    if (lista.has("mapValue")) {
                                        mapValue = lista.getJSONObject("mapValue");
                                        JSONObject fields2;
                                        if (mapValue.has("fields")) {
                                            fields2 = mapValue.getJSONObject("fields");
                                            int j = 1;
                                            for ( ; ; ) {
                                                String elementListe = "";
                                                if (fields2.has(String.valueOf(j))) { // uzimamo prvu poziciju u kvizu
                                                    elementListe = j + ". ";
                                                    JSONObject kljuc = fields2.getJSONObject(String.valueOf(j));
                                                    if (kljuc.has("mapValue")) {
                                                        JSONObject mapValueVrijednost = kljuc.getJSONObject("mapValue");
                                                        if (mapValueVrijednost.has("fields")) {
                                                            JSONObject fields3 = mapValueVrijednost.getJSONObject("fields");
                                                            String imeIgraca = fields3.names().toString();
                                                            imeIgraca = imeIgraca.replace("[", "");
                                                            imeIgraca = imeIgraca.replace("\"", "");
                                                            imeIgraca = imeIgraca.replace("]", "");
                                                            elementListe += imeIgraca + ": ";
                                                            JSONObject vrijednostObjekat = fields3.getJSONObject(imeIgraca);
                                                            double procenat = vrijednostObjekat.getDouble("doubleValue");
                                                            elementListe += procenat + "%";
                                                        }
                                                    }
                                                } else break;
                                                podaciOIgrama.add(elementListe);
                                                ++j;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute (String res) {
        delegat.processFinish(res);
    }

}