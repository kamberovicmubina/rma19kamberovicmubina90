package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class Pitanje implements Parcelable {
    private String naziv;
    private String tekstPitanja;
    private ArrayList<String> odgovori;
    private String tacan;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Pitanje createFromParcel(Parcel in) {
            return new Pitanje(in);
        }

        public Pitanje[] newArray(int size) {
            return new Pitanje[size];
        }
    };

    public Pitanje() {
    }

    public Pitanje(String naziv, String tekstPitanja, ArrayList<String> odgovori, String tacan) {
        this.naziv = naziv;
        this.tekstPitanja = tekstPitanja;
        this.odgovori = odgovori;
        this.tacan = tacan;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getTekstPitanja() {
        return tekstPitanja;
    }

    public void setTekstPitanja(String tekstPitanja) {
        this.tekstPitanja = tekstPitanja;
    }

    public ArrayList<String> getOdgovori() {
        return odgovori;
    }

    public void setOdgovori(ArrayList<String> odgovori) {
        this.odgovori = odgovori;
    }

    public String getTacan() {
        return tacan;
    }

    public void setTacan(String tacan) {
        this.tacan = tacan;
    }

    public ArrayList<String> dajRandomOdgovore () {
        ArrayList<String> randomOdgovori = odgovori;
        Collections.shuffle(randomOdgovori);
        return randomOdgovori;
    }

    public String toString() {
        return naziv;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.naziv);
        dest.writeString(this.tekstPitanja);
        dest.writeString(this.tacan);
        dest.writeList(this.odgovori);
    }

    public Pitanje(Parcel in){
        this.naziv = in.readString();
        this.tekstPitanja = in.readString();
        this.tacan =  in.readString();
        this.odgovori = in.readArrayList(String.class.getClassLoader());
    }
}
