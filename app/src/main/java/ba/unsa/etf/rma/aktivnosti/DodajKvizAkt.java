package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.opencsv.CSVReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.task.UcitajPodatkeIzBazeTask;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorijeBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kvizoviBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanjaBaza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.siriEkran;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.connected;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.sviKvizovi;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.aktivnosti.DodajKategorijuAkt.novaKategorija;
import static ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt.novoPitanje;


public class DodajKvizAkt extends AppCompatActivity {
    private static final int READ_REQUEST_CODE = 44;
    private static final String TAG = DodajKvizAkt.class.getName();
    private Spinner spinner;
    private ListView lvDodana, lvMoguca;
    private EditText tekstNazivKviza;
    private Button dodajBtn;
    private Button btnImport;
    private Kviz noviKviz = null;
    Resources res;
    ArrayAdapter<String> adapter;
    ArrayAdapter<Pitanje> adapterZaListuDodanih;
    ArrayAdapter<Pitanje> adapterZaListuMogucih;
    private ArrayList<Pitanje> mogucaPitanja = new ArrayList<>();
    ArrayList<Pitanje> pitanjaUKvizu = new ArrayList<>();
    ArrayList<String> naziviSvihKategorija = null;
    String nazivKviza = "";
    Kategorija kategorija = new Kategorija();

    public class DodajKvizTask extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            GoogleCredential credentials = null;
            try {
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                Log.d("TOKEN", TOKEN);
                String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Kvizovi?documentId=" + noviKviz.getNaziv() + "&access_token=";
                HttpURLConnection conn = null;
                try {
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                    conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                    String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + noviKviz.getNaziv() + "\"}, \"idKategorije\": {\"stringValue\":\"";
                    if (noviKviz.getKategorija() != null) {
                        dokument = dokument + noviKviz.getKategorija().getNaziv();
                    } else {
                        dokument = dokument + "null";
                    }
                    dokument = dokument + "\"}, " + " \"pitanja\": {\"arrayValue\":";
                    if (noviKviz.getPitanja() != null) {
                        dokument = dokument + " {\"values\": [";
                        for (int i = 0; i < noviKviz.getPitanja().size(); i++) {
                            if (i != noviKviz.getPitanja().size() - 1) {
                                dokument = dokument + "{\"stringValue\":\"" + noviKviz.getPitanja().get(i).getNaziv() + "\"}, ";
                            } else {
                                dokument = dokument + "{\"stringValue\":\"" + noviKviz.getPitanja().get(i).getNaziv() + "\"}";
                            }
                        }
                        dokument += "]}}}}";
                    } else {
                        dokument = dokument + "null" + "}}}";
                    }

                    try (OutputStream os = conn.getOutputStream()) {
                        byte[] input = dokument.getBytes(StandardCharsets.UTF_8);
                        os.write(input, 0, input.length);
                    }
                    int code = conn.getResponseCode();
                    InputStream odgovor = conn.getInputStream();
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, StandardCharsets.UTF_8))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        Log.d("ODGOVOR", response.toString());
                    }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class IzmijeniKvizTask extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            GoogleCredential credentials = null;
            try {
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Kvizovi/"+ trenutniKviz.getId() + "?access_token=";
                HttpURLConnection conn = null;
                try {
                    URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                    conn = (HttpURLConnection) urlObj.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("PATCH");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + trenutniKviz.getNaziv() + "\"}, \"idKategorije\": {\"stringValue\":\"";
                if (trenutniKviz.getKategorija() != null) {
                    dokument = dokument + trenutniKviz.getKategorija().getNaziv();
                } else {
                    dokument = dokument + "null";
                }
                dokument = dokument + "\"}, " + " \"pitanja\": {\"arrayValue\":";
                if (trenutniKviz.getPitanja() != null) {
                    dokument = dokument + " {\"values\": [";
                    for (int i = 0; i < trenutniKviz.getPitanja().size(); i++) {
                        if (i != trenutniKviz.getPitanja().size()-1) {
                            dokument = dokument + "{\"stringValue\":\"" + trenutniKviz.getPitanja().get(i).getNaziv() + "\"}, ";
                        } else {
                            dokument = dokument + "{\"stringValue\":\"" + trenutniKviz.getPitanja().get(i).getNaziv() + "\"}";
                        }
                    }
                    dokument += "]}}}}";
                } else {
                    dokument = dokument + "null" + "}}}";
                }

                try (OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes(StandardCharsets.UTF_8);
                    os.write(input, 0, input.length);
                }
                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, StandardCharsets.UTF_8))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    Log.d("ODGOVOR UREDJIVANJA", response.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kviz);
        res = getResources();
        kategorija = new Kategorija();
        View footerViewDodana = null;

        spinner = findViewById(R.id.spKategorije);
        lvDodana = findViewById(R.id.lvDodanaPitanja);
        lvMoguca = findViewById(R.id.lvMogucaPitanja);
        tekstNazivKviza = findViewById(R.id.etNaziv);
        dodajBtn = findViewById(R.id.btnDodajKviz);
        btnImport = findViewById(R.id.btnImportKviz);

        mogucaPitanja = getIntent().getParcelableArrayListExtra("mogucaPitanja");
        if (mogucaPitanja == null) mogucaPitanja = new ArrayList<>();
        adapterZaListuMogucih = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mogucaPitanja);
        lvMoguca.setAdapter(adapterZaListuMogucih);
        connected = spojenoNaMrezu();
        if (connected) {
            if (trenutniKviz == null) {
                mogucaPitanja = new ArrayList<>();
                new UcitajPodatkeIzBazeTask(new AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        mogucaPitanja.addAll(pitanjaBaza);
                        adapterZaListuMogucih.notifyDataSetChanged();
                    }
                }, this).execute(" ");

            } else {
                new UcitajPodatkeIzBazeTask(new AsyncResponse() {
                    @Override
                    public void processFinish(String output) {

                        if (pitanjaBaza != null && pitanjaBaza.size() != 0) {
                            mogucaPitanja.clear();
                            if (trenutniKviz.getPitanja() != null) {
                                for (Pitanje pBaza : pitanjaBaza) {
                                    boolean pitanjeUKvizu = false;
                                    for (Pitanje p : trenutniKviz.getPitanja()) {
                                        if (pBaza.getNaziv().equals(p.getNaziv())) {
                                            pitanjeUKvizu = true;
                                            break;
                                        }
                                    }
                                    if (!pitanjeUKvizu) mogucaPitanja.add(pBaza);
                                }
                            } else {
                                mogucaPitanja.addAll(pitanjaBaza);
                            }
                        }
                        adapterZaListuMogucih.notifyDataSetChanged();
                    }
                }, this).execute(" ");
            }
        } else Toast.makeText(DodajKvizAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();

        naziviSvihKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");
        nazivKviza = getIntent().getStringExtra("nazivKviza");
        tekstNazivKviza.setText(nazivKviza);
        pitanjaUKvizu = getIntent().getParcelableArrayListExtra("pitanjaUKvizu");
        if (trenutniKviz != null) pitanjaUKvizu = trenutniKviz.getPitanja();
        if (pitanjaUKvizu == null) pitanjaUKvizu = new ArrayList<>();
        adapterZaListuDodanih = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, pitanjaUKvizu);
        lvDodana.setAdapter(adapterZaListuDodanih);
        footerViewDodana = getLayoutInflater().inflate(R.layout.footer_view_question, null);
        lvDodana.addFooterView(footerViewDodana);

        boolean ima = false;
        for (String s : naziviSvihKategorija) {
            if (s.equals("Dodaj kategoriju")){
                ima = true;
                break;
            }
        }
        if (!ima) naziviSvihKategorija.add("Dodaj kategoriju");

        if (naziviSvihKategorija != null && naziviSvihKategorija.size() == 1 && trenutniKviz == null) { // dosli prvi put u ovu aktivnost, u nazivima ima samo "Sve kategorije"
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, naziviSvihKategorija);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            int defaultPozicija = adapter.getPosition("Sve kategorije");
            spinner.setAdapter(adapter);
            spinner.setSelection(defaultPozicija);
        } else if (naziviSvihKategorija != null && trenutniKviz == null) {
            naziviSvihKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");
        } else if (naziviSvihKategorija != null){ // ako je odabran neki kviz
            naziviSvihKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");
            kategorija = (Kategorija) getIntent().getSerializableExtra("novaKategorija");
            if (trenutniKviz != null) {
                tekstNazivKviza.setText(trenutniKviz.getNaziv());

            }
        }
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, naziviSvihKategorija);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //kategorija = (Kategorija) getIntent().getSerializableExtra("novaKategorija");
        final String nazivKat = getIntent().getStringExtra("nazivKategorije");

        if (nazivKat != null) {
            int pozicijaNoveKategorije = adapter.getPosition(nazivKat);
            spinner.setSelection(pozicijaNoveKategorije);
        }
        if (trenutniKviz != null && trenutniKviz.getKategorija() != null) {
            int pozicijaNoveKategorije = adapter.getPosition(trenutniKviz.getKategorija().getNaziv());
            spinner.setSelection(pozicijaNoveKategorije);
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Sve kategorije")) {
                    //ne radi nista
                } else if (parent.getItemAtPosition(position).equals("Dodaj kategoriju")){
                    connected = spojenoNaMrezu();
                    Log.d("KONEKCIJA", String.valueOf(connected));
                    if (connected) {
                        Intent dodajKategorijuIntent = new Intent(DodajKvizAkt.this, DodajKategorijuAkt.class);
                        dodajKategorijuIntent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                        dodajKategorijuIntent.putExtra("nazivKviza", tekstNazivKviza.getText().toString());
                        dodajKategorijuIntent.putExtra("pitanjaUKvizu", pitanjaUKvizu);
                        DodajKvizAkt.this.startActivity(dodajKategorijuIntent);
                    } else {
                        Toast.makeText(DodajKvizAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dodajBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connected = spojenoNaMrezu();
                if (connected) {
                    boolean postoji = false;
                    for (Kviz k : kvizoviBaza) {
                        if (k != null && k.getNaziv().equals(tekstNazivKviza.getText().toString())) {
                            postoji = true;
                            break;
                        }
                    }
                    Kategorija kategorija = null;
                    if (naziviSvihKategorija.size() >= 2) {
                        String nazivKat = (String) spinner.getSelectedItem();
                        if (!(nazivKat.equals("Sve kategorije") || nazivKat.equals("Dodaj kategoriju"))) {
                            for (Kategorija k : kategorije) {
                                if (k != null && k.getNaziv().equals(nazivKat)) {
                                    kategorija = k;
                                    break;
                                }
                            }

                        }
                        if (!postoji) {
                            if (spinner.getSelectedItem() != null && !tekstNazivKviza.getText().toString().equals("")) {
                                Intent returnIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                                if (trenutniKviz == null) { // dodajemo novi kviz
                                    noviKviz = new Kviz();
                                    noviKviz.setNaziv(tekstNazivKviza.getText().toString());
                                    noviKviz.setKategorija(kategorija);
                                    noviKviz.setPitanja(pitanjaUKvizu);
                                    noviKviz.setId(tekstNazivKviza.getText().toString());
                                    sviKvizovi.add(noviKviz);

                                    // dodavanje kviza u bazu
                                    new DodajKvizTask().execute();
                                } else { // mijenjamo postojeci
                                    trenutniKviz.setNaziv(tekstNazivKviza.getText().toString());
                                    trenutniKviz.setKategorija(kategorija);
                                    trenutniKviz.setPitanja(pitanjaUKvizu);

                                    // mijenjanje kviza u bazi
                                    new IzmijeniKvizTask().execute();
                                }
                                returnIntent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                                returnIntent.putExtra("nazivKategorije", nazivKat);
                                returnIntent.putExtra("nazivKviza", nazivKviza);
                                DodajKvizAkt.this.startActivity(returnIntent);
                            } else if (tekstNazivKviza.getText().toString().equals("")) {
                                tekstNazivKviza.setBackgroundColor(Color.RED);
                            }
                        } else if (trenutniKviz != null) { // samo uredjujemo postojeci kviz, njegovo ime vec postoji
                            trenutniKviz.setNaziv(tekstNazivKviza.getText().toString());
                            trenutniKviz.setKategorija(kategorija);
                            trenutniKviz.setPitanja(pitanjaUKvizu);
                            new IzmijeniKvizTask().execute();
                            Intent returnIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
                            returnIntent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                            returnIntent.putExtra("nazivKategorije", nazivKat);
                            returnIntent.putExtra("nazivKviza", nazivKviza);
                            DodajKvizAkt.this.startActivity(returnIntent);
                        } else {
                            tekstNazivKviza.setBackgroundColor(Color.RED);
                        }
                        kategorije.add(kategorija);
                    }
                } else {
                    /*ContentValues values = new ContentValues();
                    values.put("KVIZ_ID", );*/
                    Toast.makeText(DodajKvizAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connected = spojenoNaMrezu();
                if (connected) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("text/*, csv/*"); // ovim osiguravamo da se mogu importovati samo datoteke ova dva tipa

                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intent, READ_REQUEST_CODE);
                    }
                } else {
                    Toast.makeText(DodajKvizAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        footerViewDodana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connected = spojenoNaMrezu();
                if (connected) {
                    Intent intent = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
                    //moramo poslati sve podatke da ih i vratimo
                    intent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
                    intent.putExtra("nazivKviza", tekstNazivKviza.getText().toString());
                    intent.putExtra("novaKategorija", kategorija);
                    intent.putExtra("nazivKategorije", nazivKat);
                    intent.putExtra("pitanjaUKvizu", pitanjaUKvizu);
                    intent.putExtra("mogucaPitanja", mogucaPitanja);
                    DodajKvizAkt.this.startActivity(intent);
                } else {
                    Toast.makeText(DodajKvizAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lvDodana.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0 && position < pitanjaUKvizu.size()) {
                    Pitanje trenutno = pitanjaUKvizu.get(position);
                    pitanjaUKvizu.remove(trenutno);
                    mogucaPitanja.add(trenutno);
                    adapterZaListuDodanih = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, pitanjaUKvizu);
                    lvDodana.setAdapter(adapterZaListuDodanih);
                    adapterZaListuMogucih = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, mogucaPitanja);
                    lvMoguca.setAdapter(adapterZaListuMogucih);
                }
            }
        });

        lvMoguca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0 && position < mogucaPitanja.size()) {
                    Pitanje trenutno = mogucaPitanja.get(position);
                    mogucaPitanja.remove(trenutno);
                    pitanjaUKvizu.add(trenutno);
                    adapterZaListuDodanih = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, pitanjaUKvizu);
                    lvDodana.setAdapter(adapterZaListuDodanih);
                    adapterZaListuMogucih = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, mogucaPitanja);
                    lvMoguca.setAdapter(adapterZaListuMogucih);
                }
            }
        });
    }

    @Override
    public void onBackPressed () {
        Kategorija kategorija = null;
        if (naziviSvihKategorija.size() >= 2) {
            String nazivKat = (String) spinner.getSelectedItem();
            if (!nazivKat.equals("Sve kategorije")) {
                for (Kategorija k : kategorije) {
                    if (k != null && k.getNaziv().equals(nazivKat)) {
                        kategorija = k;
                        break;
                    }
                }

            }
            Intent returnIntent = new Intent(DodajKvizAkt.this, KvizoviAkt.class);
            returnIntent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
            returnIntent.putExtra("nazivKategorije", nazivKat);
            returnIntent.putExtra("nazivKviza", nazivKviza);
            DodajKvizAkt.this.startActivity(returnIntent);
            kategorije.add(kategorija);
            connected = spojenoNaMrezu();
            if (connected)
                kategorijeBaza.add(kategorija);
            else Toast.makeText(DodajKvizAkt.this, "Nema internet konekcije! Kategorija nije dodana u bazu!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) { // provjeriti ovdje za konekciju
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;

            if (data != null) {
                uri = data.getData();
                try {
                    String datoteka = dajStringIzCVSa(uri);
                    final Kviz noviKviz = dajKvizIzDatoteke(datoteka);
                    if (noviKviz == null) {
                        return;
                    }
                    tekstNazivKviza.setText(noviKviz.getNaziv());
                    final boolean[] ima = {false};
                    new UcitajPodatkeIzBazeTask(new AsyncResponse() {
                        @Override
                        public void processFinish(String output) {
                            for (Kategorija k : kategorijeBaza) {
                                if ( k != null && k.getNaziv().equals(noviKviz.getKategorija().getNaziv())) {
                                    ima[0] = true;
                                    break;
                                }

                            }
                        }
                    }, this).execute(" ");

                    if (!ima[0]) {
                        novaKategorija = noviKviz.getKategorija();
                        new DodajKategorijuAkt.DodajKategorijuTask(new AsyncResponse() {
                            @Override
                            public void processFinish(String output) {
                                naziviSvihKategorija.add(noviKviz.getKategorija().getNaziv());
                                kategorije.add(noviKviz.getKategorija());
                            }
                        }, this).execute(" ");
                    }
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, naziviSvihKategorija);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                    int pozicijaNoveKategorije = adapter.getPosition(noviKviz.getKategorija().getNaziv());
                    spinner.setSelection(pozicijaNoveKategorije);
                    pitanjaUKvizu = noviKviz.getPitanja();
                    adapterZaListuDodanih = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, pitanjaUKvizu);
                    lvDodana.setAdapter(adapterZaListuDodanih);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private String dajStringIzCVSa (Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line + "\n");
        }
        inputStream.close();
        return stringBuilder.toString();

    }

    private Kviz dajKvizIzDatoteke (String datoteka) {
        Kviz kviz = new Kviz();
        final ArrayList<Pitanje> pitanja = new ArrayList<>();
        /*if (!getIntent().getType().equals("text/*") && !getIntent().getType().equals("csv/*")) {
            AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Datoteka kviza kojeg importujete nema ispravan format!");
            upozorenje.show();
            return null;
        }*/

        String[] tekst = datoteka.split("\n");
        if (tekst.length != 0) {
            String[] prviRed = tekst[0].split(",");
            if (prviRed.length != 3) { // provjera da li u prvom redu postoji jos nesto
                AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete nema ispravan format!").setPositiveButton("OK", null);
                upozorenje.show();
                return null;
            }
            boolean postojiNazivKviza = false;
            if (sviKvizovi != null && sviKvizovi.size() != 0) {
                for (Kviz k : sviKvizovi) {
                    if (k.getNaziv().equals(prviRed[0])) {
                        postojiNazivKviza = true;
                        break;
                    }
                }
            }
            if (!postojiNazivKviza) {
                kviz.setNaziv(prviRed[0]);
            } else {
                AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete vec postoji!").setPositiveButton("OK", null);
                upozorenje.show();
                return null;
            }

            if (Integer.parseInt(prviRed[2]) != (tekst.length-1)) {
                AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete ima neispravan broj pitanja!").setPositiveButton("OK", null);
                upozorenje.show();
                return null;
            }

            kategorija = new Kategorija();
            kategorija.setNaziv(prviRed[1]);
            kategorija.setId("0");
            kviz.setKategorija(kategorija);

            int i = 1;
            while (i < tekst.length) {
                String[] red = tekst[i].split(",");
                if (red.length == 3) {// nije dozvoljen format da pise broj odgovora i indeks tacnog odgovora, a da nema teksta nijednog odgovora
                    AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete nema ispravan format!").setPositiveButton("OK", null);
                    upozorenje.show();
                    return null;
                }
                ArrayList<String> odgovori = new ArrayList<>();
                final Pitanje p = new Pitanje();
                if (Integer.parseInt(red[1]) != red.length - 3) {
                    AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete ima neispravan broj odgovora!").setPositiveButton("OK", null);
                    upozorenje.show();
                    return null;
                }
                try {
                    Integer broj = Integer.parseInt(red[2]); // ako nije cijeli broj bacit ce gresku
                } catch (NumberFormatException e) {
                    AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete ima neispravan index tacnog odgovora!").setPositiveButton("OK", null);
                    upozorenje.show();
                    return null;
                }
                if (Integer.parseInt(red[2]) < 0 || Integer.parseInt(red[2]) >= red.length - 3) {
                    AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete ima neispravan index tacnog odgovora!").setPositiveButton("OK", null);
                    upozorenje.show();
                    return null;
                }

                boolean postojiOdg = false;
                for (int j = 3; j < red.length; j++) { // prolazimo kroz tekstove svih odgovora
                    String odg = red[j];
                    postojiOdg = false;
                    for (int k = j+1; k < red.length; k++) {
                        if (odg.equals(red[k])) {
                            postojiOdg = true;
                            break;
                        }
                    }
                    if (postojiOdg) break;
                }
                if (postojiOdg) {
                    AlertDialog.Builder upozorenje = new AlertDialog.Builder(this).setMessage("Kviz kojeg importujete nije ispravan postoji ponavljanje odgovora!").setPositiveButton("OK", null);
                    upozorenje.show();
                    return null;
                }

                p.setNaziv(red[0]);
                p.setTekstPitanja(red[0]);
                int j = 3;
                while (j < red.length) {
                    odgovori.add(red[j]);
                    if (j-3 == Integer.parseInt(red[2])) p.setTacan(red[j]);
                    j++;
                }
                p.setOdgovori(odgovori);
                pitanja.add(p);
                novoPitanje = p;
                new DodajPitanjeAkt.DodajPitanjeTask(new AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        pitanja.add(p);
                    }
                }, DodajKvizAkt.this).execute(" ");
                kviz.setPitanja(pitanja);
                i++;
            }
        }
        return kviz;
    }

    public boolean spojenoNaMrezu() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
            return true;
        return false;
    }
}
