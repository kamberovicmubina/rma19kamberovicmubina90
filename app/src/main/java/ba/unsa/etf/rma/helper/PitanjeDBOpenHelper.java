package ba.unsa.etf.rma.helper;


public class PitanjeDBOpenHelper {
    public static final String DATABASE_TABLE_PITANJA = "pitanja";
    public static final String PITANJE_ID = "_id";
    public static final String PITANJE_NAZIV = "nazivPitanja";
    public static final String INDEX_TACNOG = "indexTacnog";

    public static final String DATABASE_TABLE_ODGOVORI = "odgovori";
    public static final String ODGOVOR_ID = "_id";
    public static final String ODGOVOR_NAZIV = "odgovor";

    public static final String DATABASE_TABLE_ODGOVOR_PITANJE = "odgovorPitanje";
    public static final String ODGOVOR_PITANJE_IDODGOVORA = "IdOdgovora";
    public static final String ODGOVOR_PITANJE_IDPITANJA = "idPitanja";

    public static final String CREATE_PITANJA = "CREATE TABLE " + DATABASE_TABLE_PITANJA +
            " ( " + PITANJE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PITANJE_NAZIV + " TEXT, " +
            INDEX_TACNOG + " TEXT NOT NULL);";

    public static final String CREATE_ODGOVORI = "CREATE TABLE " + DATABASE_TABLE_ODGOVORI +
            " ( " + ODGOVOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + ODGOVOR_NAZIV + " TEXT);";

    public static final String CREATE_ODGOVOR_PITANJA = "CREATE TABLE " + DATABASE_TABLE_ODGOVOR_PITANJE +
            " ( " + ODGOVOR_PITANJE_IDODGOVORA + " TEXT NOT NULL, "+
            ODGOVOR_PITANJE_IDPITANJA + " TEXT NOT NULL, " +
            "CONSTRAINT constr_tab_idOdgovora FOREIGN KEY ( " + ODGOVOR_PITANJE_IDODGOVORA + ") REFERENCES " + DATABASE_TABLE_ODGOVORI + " ( " + ODGOVOR_ID + " ), "+
            "CONSTRAINT constr_tab_idPitanja FOREIGN KEY ( " + ODGOVOR_PITANJE_IDPITANJA + ") REFERENCES " + DATABASE_TABLE_PITANJA + " ( " + PITANJE_ID + " ) "
            +");";


}
