package ba.unsa.etf.rma.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.annotation.Nullable;

import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.Ranglista;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.sviKvizovi;
import static ba.unsa.etf.rma.helper.KategorijaDBOpenHelper.DATABASE_TABLE_KATEGORIJE;
import static ba.unsa.etf.rma.helper.KategorijaDBOpenHelper.ID_IKONE;
import static ba.unsa.etf.rma.helper.KategorijaDBOpenHelper.KATEGORIJA_ID;
import static ba.unsa.etf.rma.helper.KategorijaDBOpenHelper.KATEGORIJA_NAZIV;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.DATABASE_TABLE_KVIZOVI;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.DATABASE_TABLE_KVIZ_PITANJE;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_ID;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_ID_KATEGORIJE;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_NAZIV;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_PITANJE_IDKVIZA;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_PITANJE_IDPITANJA;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.DATABASE_TABLE_ODGOVORI;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.DATABASE_TABLE_ODGOVOR_PITANJE;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.DATABASE_TABLE_PITANJA;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.INDEX_TACNOG;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.ODGOVOR_ID;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.ODGOVOR_NAZIV;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.ODGOVOR_PITANJE_IDODGOVORA;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.ODGOVOR_PITANJE_IDPITANJA;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.PITANJE_ID;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.PITANJE_NAZIV;
import static ba.unsa.etf.rma.helper.RanglistaDBOpenHelper.DATABASE_TABLE_RANGLISTE;
import static ba.unsa.etf.rma.helper.RanglistaDBOpenHelper.RANGLISTA_IGRAC;
import static ba.unsa.etf.rma.helper.RanglistaDBOpenHelper.RANGLISTA_NAZIV_KVIZA;
import static ba.unsa.etf.rma.helper.RanglistaDBOpenHelper.RANGLISTA_REZULTAT;

public class BazaHelper extends SQLiteOpenHelper {
    public static final String BAZA_IME = "baza.sqlite";
    public static final int DATABASE_VERSION = 1;

    public BazaHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public BazaHelper (Context context) {
        super(context, BAZA_IME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(KvizDBOpenHelper.CREATE_KVIZOVI);
        db.execSQL(KvizDBOpenHelper.CREATE_KVIZOVI_PITANJA);
        db.execSQL(KategorijaDBOpenHelper.CREATE_KATEGORIJE);
        db.execSQL(PitanjeDBOpenHelper.CREATE_PITANJA);
        db.execSQL(PitanjeDBOpenHelper.CREATE_ODGOVORI);
        db.execSQL(PitanjeDBOpenHelper.CREATE_ODGOVOR_PITANJA);
        db.execSQL(RanglistaDBOpenHelper.CREATE_RANGLISTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KVIZOVI);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KVIZ_PITANJE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KATEGORIJE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_PITANJA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_ODGOVORI);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_ODGOVOR_PITANJE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RANGLISTE);
        onCreate(db);
    }

    public boolean postojiKviz(Kviz kviz){
        String query = "SELECT * "+ "FROM " + DATABASE_TABLE_KVIZOVI +  " WHERE " + KVIZ_NAZIV + " = '" + kviz.getNaziv() + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 1) return true; // postoji kviz
        cursor.close();
        return false;
    }
    public void dodajKviz(Kviz kviz){
        if(!postojiKviz(kviz)) {
            ContentValues noveVrijednosti = new ContentValues();
            noveVrijednosti.put(KVIZ_NAZIV, kviz.getNaziv());
            if (kviz.getKategorija() != null)
                noveVrijednosti.put(KVIZ_ID_KATEGORIJE, kviz.getKategorija().getNaziv());
            if (kviz.getPitanja() != null) {
                for (int i = 0; i < kviz.getPitanja().size(); i++) {
                    dodajPitanje(kviz.getPitanja().get(i));
                    dodajPitanjeUKviz(kviz, kviz.getPitanja().get(i));
                }
            }
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(DATABASE_TABLE_KVIZOVI, null, noveVrijednosti);
            Log.d("IZ HELPERA", "DODAN KVIZ");
        } else Log.d("IZ HELPERA", "POSTOJI KVIZ");
    }

    public boolean postojiPitanjeUKvizu (Kviz kviz, Pitanje pitanje) {
        String upit = "SELECT * "+ "FROM " + DATABASE_TABLE_KVIZ_PITANJE + " WHERE " + KVIZ_PITANJE_IDKVIZA + " = '" + kviz.getNaziv() + "' AND " + KVIZ_PITANJE_IDPITANJA + " = '"+pitanje.getNaziv()+"';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);
        if (cursor.getCount() > 1) return true;
        cursor.close();
        return false;
    }

    public void dodajPitanjeUKviz (Kviz kviz, Pitanje pitanje) {
        if(!postojiPitanjeUKvizu(kviz, pitanje)) {
            ContentValues noveVrijednosti = new ContentValues();
            noveVrijednosti.put(KVIZ_PITANJE_IDKVIZA, kviz.getNaziv());
            noveVrijednosti.put(KVIZ_PITANJE_IDPITANJA, pitanje.getNaziv());
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(DATABASE_TABLE_KVIZ_PITANJE, null, noveVrijednosti);
        }
    }

    public boolean postojiKategorija (Kategorija kategorija) {
        String upit = "SELECT * " + "FROM " + DATABASE_TABLE_KATEGORIJE + " WHERE " + KATEGORIJA_NAZIV + " = '" + kategorija.getNaziv() + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);
        if (cursor.getCount() > 1) return true; // postoji kategorija
        cursor.close();
        return false;
    }

    public void dodajKategoriju (Kategorija kategorija) {
        if (!postojiKategorija(kategorija)) {
            ContentValues noveVrijednosti = new ContentValues();
            noveVrijednosti.put(KATEGORIJA_NAZIV, kategorija.getNaziv());
            noveVrijednosti.put(ID_IKONE, kategorija.getId());
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(DATABASE_TABLE_KATEGORIJE, null, noveVrijednosti);
        }
    }

    public boolean postojiPitanje(Pitanje pitanje){
        String upit = "SELECT * "+ "FROM " + DATABASE_TABLE_PITANJA + " WHERE " + PITANJE_NAZIV + " = '" + pitanje.getNaziv() + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);
        if (cursor.getCount() > 1) return true; // postoji pitanje
        cursor.close();
        return false;
    }

    public void dodajPitanje(Pitanje pitanje){
        if(!postojiPitanje(pitanje)) {
            ContentValues noveVrijednosti = new ContentValues();
            noveVrijednosti.put(PITANJE_NAZIV, pitanje.getNaziv());
            noveVrijednosti.put(INDEX_TACNOG, pitanje.getTacan());
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(DATABASE_TABLE_PITANJA, null, noveVrijednosti);
            if (pitanje.getOdgovori() != null) {
                for (int i = 0; i < pitanje.getOdgovori().size(); i++) {
                    dodajOdgovor(pitanje.getOdgovori().get(i));
                    dodajOdgovorUPitanje(pitanje, pitanje.getOdgovori().get(i));
                }
            }
        }
    }

    public boolean postojiOdgovorUPitanju (Pitanje pitanje, String odgovor) {
        String upit = "SELECT * "+ "FROM " + DATABASE_TABLE_ODGOVOR_PITANJE + " WHERE " + ODGOVOR_PITANJE_IDODGOVORA + " = '" + odgovor + "' AND " + ODGOVOR_PITANJE_IDPITANJA + " = '" + pitanje.getNaziv() + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);
        if(cursor.getCount() > 1) return true;
        cursor.close();
        return false;
    }

    public void dodajOdgovorUPitanje (Pitanje pitanje, String odgovor) {
        if(!postojiOdgovorUPitanju(pitanje, odgovor)) {
            ContentValues noveVrijednosti = new ContentValues();
            noveVrijednosti.put(ODGOVOR_PITANJE_IDODGOVORA, odgovor);
            noveVrijednosti.put(ODGOVOR_PITANJE_IDPITANJA, pitanje.getNaziv());
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(DATABASE_TABLE_ODGOVOR_PITANJE, null, noveVrijednosti);
        }
    }

    public boolean postojiOdgovor(String odgovor){
        String upit = "SELECT * " + "FROM " + DATABASE_TABLE_ODGOVORI + " WHERE " + ODGOVOR_NAZIV + " = '" + odgovor + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);
        if(cursor.getCount() > 1) return true;
        cursor.close();
        return false;
    }

    public void dodajOdgovor(String odgovor) {
        if(!postojiOdgovor(odgovor)) {
            ContentValues noveVrijednosti = new ContentValues();
            noveVrijednosti.put(ODGOVOR_NAZIV, odgovor);
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(DATABASE_TABLE_ODGOVORI, null, noveVrijednosti);
        }
    }

    public void urediKviz (Kviz stariKviz, Kviz noviKviz){
        ContentValues editValues = new ContentValues();
        //ubacivanje u bazu
        editValues.put(KVIZ_NAZIV, noviKviz.getNaziv());
        editValues.put(KVIZ_ID_KATEGORIJE, noviKviz.getKategorija().getNaziv());
        //brisanje iz zajednicke tabele(kviz_pitanje)
        if (stariKviz.getPitanja() != null) {
            for (int i = 0; i < stariKviz.getPitanja().size(); i++) {
                String where = KVIZ_PITANJE_IDPITANJA + " = '" + stariKviz.getPitanja().get(i).getNaziv() + "';";
                String[] whereArgs = null;
                SQLiteDatabase db = this.getWritableDatabase();
                db.delete(DATABASE_TABLE_KVIZ_PITANJE, where, whereArgs);
            }
        }
        //dodvanje opet
        if (noviKviz.getPitanja() != null) {
            for (int i = 0; i < noviKviz.getPitanja().size(); i++) {
                dodajPitanje(noviKviz.getPitanja().get(i));
                dodajPitanjeUKviz(noviKviz, noviKviz.getPitanja().get(i));
            }
        }
        String where = KVIZ_ID + " = '" + stariKviz.getNaziv() + "';";
        String[] whereArgs = null;
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(DATABASE_TABLE_KVIZOVI, editValues, where, whereArgs);
    }

    public ArrayList<Kviz> ucitajKvizove(){
        ArrayList<Kviz> sviKvizovi = new ArrayList<>();
        String[] kolone = new String[]{KVIZ_NAZIV, KVIZ_ID_KATEGORIJE};
        String whereUslov = null;
        String[] whereArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_KVIZOVI, kolone, whereUslov, whereArgs, groupBy, having, orderBy);

        int KOLONA_KVIZ_NAZIV = cursor.getColumnIndexOrThrow(KVIZ_NAZIV);
        int KOLONA_KVIZ_IDKATEGORIJE = cursor.getColumnIndexOrThrow(KVIZ_ID_KATEGORIJE);

        while(cursor.moveToNext()) {
            Kviz novi = new Kviz();
            novi.setNaziv(cursor.getString(KOLONA_KVIZ_NAZIV));
            novi.setKategorija(dajKategoriju(cursor.getString(KOLONA_KVIZ_IDKATEGORIJE)));
            novi.setPitanja(ucitajPitanja(novi));
            sviKvizovi.add(novi);
        }
        cursor.close();
        return sviKvizovi;
    }

    public Kategorija dajKategoriju(String naziv){
        String query = "SELECT * " + " FROM " + DATABASE_TABLE_KATEGORIJE + " WHERE " + KATEGORIJA_NAZIV + " = '" + naziv + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int KOLONA_KATEGORIJA_IDIKONICE = cursor.getColumnIndexOrThrow(ID_IKONE);

        Kategorija kategorija = new Kategorija();
        kategorija.setNaziv(naziv);
        while(cursor.moveToNext()) {
            kategorija.setId(cursor.getString(KOLONA_KATEGORIJA_IDIKONICE));
        }
        cursor.close();
        return kategorija;
    }

    public ArrayList<Kategorija> ucitajKategorije(){
        ArrayList<Kategorija> sveKategorije = new ArrayList<>();
        String[] kolone = new String[]{KATEGORIJA_NAZIV, ID_IKONE};
        String whereUslov = null;
        String[] whereArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_KATEGORIJE, kolone, whereUslov, whereArgs, groupBy, having, orderBy);

        int KOLONA_KATEGORIJA_NAZIV = cursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
        int KOLONA_KATEGORIJA_IDIKONE = cursor.getColumnIndexOrThrow(ID_IKONE);

        while(cursor.moveToNext()) {
            Kategorija nova = new Kategorija();
            nova.setNaziv(cursor.getString(KOLONA_KATEGORIJA_NAZIV));
            nova.setId(cursor.getString(KOLONA_KATEGORIJA_IDIKONE));
            sveKategorije.add(nova);
        }
        cursor.close();
        return sveKategorije;
    }

    public ArrayList<Pitanje> ucitajPitanja(Kviz kviz){
        ArrayList<Pitanje> pitanjaUKvizu = new ArrayList<>();
        String query = "SELECT * " + "FROM " + DATABASE_TABLE_KVIZ_PITANJE + " WHERE " + KVIZ_PITANJE_IDKVIZA + " = '" + kviz.getNaziv() +"';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int KOLONA_PITANJE = cursor.getColumnIndexOrThrow(KVIZ_PITANJE_IDPITANJA);

        while(cursor.moveToNext()) {
            pitanjaUKvizu.add(dajPitanje(cursor.getString(KOLONA_PITANJE)));
        }
        cursor.close();
        return pitanjaUKvizu;
    }

    public Pitanje dajPitanje(String naziv){
        String query = "SELECT * " + " FROM " + DATABASE_TABLE_PITANJA + " WHERE " + PITANJE_NAZIV + " = '" + naziv + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int KOLONA_PITANJE_TACANODGOVOR = cursor.getColumnIndexOrThrow(INDEX_TACNOG);

        Pitanje pitanje = new Pitanje();
        while(cursor.moveToNext()) {
            pitanje.setTacan(cursor.getString(KOLONA_PITANJE_TACANODGOVOR));
        }
        pitanje.setNaziv(naziv);
        pitanje.setTekstPitanja(naziv);
        pitanje.setOdgovori(ucitajOdgovore(naziv));
        cursor.close();
        return pitanje;
    }

    public ArrayList<String> ucitajOdgovore(String naziv){
        ArrayList<String> sviOdgovoriNaPitanje = new ArrayList<>();
        String query = "SELECT * " + "FROM " + DATABASE_TABLE_ODGOVOR_PITANJE + " WHERE " + ODGOVOR_PITANJE_IDPITANJA + " = '" + naziv +"';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int KOLONA_ODGOVOR = cursor.getColumnIndexOrThrow(ODGOVOR_PITANJE_IDODGOVORA);

        while(cursor.moveToNext()) {
            sviOdgovoriNaPitanje.add(cursor.getString(KOLONA_ODGOVOR));
        }
        cursor.close();
        return sviOdgovoriNaPitanje;
    }

    public ArrayList<Ranglista> ucitajRangliste(){
        ArrayList<Ranglista> rangLista = new ArrayList<>();
        String query = "SELECT * FROM "+ DATABASE_TABLE_RANGLISTE + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int KOLONA_RANGLISTA_REZULTAT = cursor.getColumnIndexOrThrow(RANGLISTA_REZULTAT);
        int KOLONA_RANGLISTA_NAZIV_KVIZA = cursor.getColumnIndexOrThrow(RANGLISTA_NAZIV_KVIZA);
        int KOLONA_RANGLISTA_IGRAC = cursor.getColumnIndexOrThrow(RANGLISTA_IGRAC);

        while(cursor.moveToNext()) {
            rangLista.add(new Ranglista(cursor.getString(KOLONA_RANGLISTA_NAZIV_KVIZA), cursor.getString(KOLONA_RANGLISTA_IGRAC), cursor.getDouble(KOLONA_RANGLISTA_REZULTAT)));
        }
        Collections.sort(rangLista, new Comparator<Ranglista>() {
            @Override
            public int compare(Ranglista ranglista, Ranglista t1) {
                if(ranglista.rezultat > t1.rezultat) return 1;
                else if(ranglista.rezultat < t1.rezultat) return -1;
                return 0;
            }
        });
        cursor.close();
        return rangLista;
    }

    public void dodajURanglistu(String kviz, String igrac, double rezultat) {
        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(RANGLISTA_NAZIV_KVIZA, kviz);
        noveVrijednosti.put(RANGLISTA_REZULTAT, rezultat);
        noveVrijednosti.put(RANGLISTA_IGRAC, igrac);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DATABASE_TABLE_RANGLISTE, null, noveVrijednosti);
    }

    public void closeDatabase() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void obrisiSve(){
        onUpgrade(this.getWritableDatabase(),0,0);
    }

}
