package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.helper.BazaHelper;
import ba.unsa.etf.rma.klase.CustomAdapter;
import ba.unsa.etf.rma.klase.KalendarResolver;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;
import ba.unsa.etf.rma.klase.Ranglista;
import ba.unsa.etf.rma.task.DodajURanglistuTask;
import ba.unsa.etf.rma.task.FiltrirajKvizoveTask;
import ba.unsa.etf.rma.task.UcitajPodatkeIzBazeTask;
import static ba.unsa.etf.rma.fragmenti.RangLista.podaciOIgrama;


public class KvizoviAkt extends AppCompatActivity implements ListaFrag.onItemClick {
    private Spinner spinner;
    private ListView lista;
    private KvizoviAkt mainActivity = null;
    public static ArrayList<Kategorija> kategorije = new ArrayList<>();
    public static ArrayList<Kviz> sviKvizovi = new ArrayList<>();
    private ArrayList<Kviz> prikazaniKvizovi = new ArrayList<>();
    private ArrayList<String> naziviKategorija = new ArrayList<>();
    private ArrayList<Pitanje> pitanjaUKvizu = new ArrayList<>();
    private CustomAdapter customAdapter;
    private ArrayAdapter<String> adapter;
    Resources res;
    View footerView;
    public static Kviz trenutniKviz = null;
    public static ArrayList<Kviz> kvizoviBaza = new ArrayList<>();
    public static ArrayList<Kategorija> kategorijeBaza = new ArrayList<>();
    public static ArrayList<Pitanje> pitanjaBaza = new ArrayList<>();
    public static ArrayList<Kviz> filtriraniKvizovi = new ArrayList<>();
    BazaHelper bazaHelper;
    public static boolean connected;
    public static boolean siriEkran = false;
    FrameLayout listP, detailP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bazaHelper = new BazaHelper(KvizoviAkt.this);

        listP = findViewById(R.id.listPlace);
        siriEkran = listP != null;
        naziviKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");

        if (siriEkran) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ListaFrag lf = new ListaFrag();
            DetailFrag df = new DetailFrag();
            Bundle bundle = new Bundle();
            if (naziviKategorija == null) {
                naziviKategorija = new ArrayList<>();
                naziviKategorija.add(0, "Sve kategorije");
            }
            bundle.putStringArrayList("naziviSvihKategorija", naziviKategorija);
            if (trenutniKviz != null && trenutniKviz.getKategorija() != null) {
                bundle.putString("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
                bundle.putString("nazivKviza", trenutniKviz.getNaziv());
            }
            df.setArguments(bundle);
            ft.replace(R.id.listPlace, lf);
            ft.replace(R.id.detailPlace, df).commit();
        } else {
            mainActivity = this;
            res = getResources();
            spinner = findViewById(R.id.spPostojeceKategorije);
            lista = findViewById(R.id.lvKvizovi);
            /*if (connected) {
                new UcitajPodatkeIzBazeTask(new AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        prikazaniKvizovi.clear();
                        sviKvizovi.clear();
                        kategorije.clear();

                        prikazaniKvizovi.addAll(kvizoviBaza);
                        sviKvizovi.addAll(kvizoviBaza);
                        customAdapter.notifyDataSetChanged();

                        kategorije.addAll(kategorijeBaza);
                        if (naziviKategorija == null) naziviKategorija = new ArrayList<>();
                        naziviKategorija.clear();
                        naziviKategorija.add("Sve kategorije");
                        if (kategorije != null && kategorije.size() != 0) {
                            for (Kategorija k : kategorije) {
                                if (k != null)
                                    naziviKategorija.add(k.getNaziv());
                            }
                        }
                        for (Kviz k : kvizoviBaza) {
                            bazaHelper.dodajKviz(k);
                            Log.d("IZ APP", "proslo");
                            if (k.getPitanja() != null && k.getPitanja().size() != 0) {
                                for (int i = 0; i < k.getPitanja().size(); i++) {
                                    bazaHelper.dodajPitanje(k.getPitanja().get(i));
                                    for (int j = 0; j < k.getPitanja().get(i).getOdgovori().size(); j++) {
                                        bazaHelper.dodajOdgovor(k.getPitanja().get(i).getOdgovori().get(j));
                                    }
                                }
                            }
                        }
                        for (Kategorija k : kategorijeBaza) {
                            bazaHelper.dodajKategoriju(k);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, this).execute(" ");
            } else {
                // ucitaj podatke iz sqlite baze
                sviKvizovi = bazaHelper.ucitajKvizove();
                prikazaniKvizovi = bazaHelper.ucitajKvizove();
                kategorije = bazaHelper.ucitajKategorije();
                if (naziviKategorija == null) naziviKategorija = new ArrayList<>();
                naziviKategorija.clear();
                naziviKategorija.add("Sve kategorije");
                if (kategorije != null && kategorije.size() != 0) {
                    for (Kategorija k : kategorije) {
                        if (k != null)
                            naziviKategorija.add(k.getNaziv());
                    }
                }
                Toast.makeText(KvizoviAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
            }*/

            customAdapter = new CustomAdapter(this, prikazaniKvizovi);
            lista.setAdapter(customAdapter);
            footerView = getLayoutInflater().inflate(R.layout.footer_view_quiz, null);
            lista.addFooterView(footerView);

            if (naziviKategorija == null) { // inicijalni display, tek se pokrenula aplikacija
                naziviKategorija = new ArrayList<>();
                naziviKategorija.add(0, "Sve kategorije"); // nema drugih kategorija za dodati na samom pocetku
            } else if (naziviKategorija.size() >= 2) {
                naziviKategorija.remove("Dodaj kategoriju");
            }
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, naziviKategorija);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(0);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                    if (position >= 0 && position < naziviKategorija.size()) {
                        connected = spojenoNaMrezu();
                        Log.d("KONEKCIJA", String.valueOf(connected));
                        if (connected) {
                            if (position == 0) {
                                prikazaniKvizovi.clear();
                                prikazaniKvizovi.addAll(kvizoviBaza);
                                customAdapter = new CustomAdapter(KvizoviAkt.this, prikazaniKvizovi);
                                lista.setAdapter(customAdapter);
                            } else if (naziviKategorija.get(position) != null) {
                                new FiltrirajKvizoveTask(new AsyncResponse() {
                                    @Override
                                    public void processFinish(String output) {
                                        prikazaniKvizovi.clear();
                                        if (naziviKategorija.get(position).equals("Sve kategorije")) {
                                            prikazaniKvizovi.addAll(kvizoviBaza);
                                        } else {
                                            prikazaniKvizovi.addAll(filtriraniKvizovi);
                                        }
                                        customAdapter.notifyDataSetChanged();
                                    }
                                }, KvizoviAkt.this).execute(naziviKategorija.get(position));
                            }
                        } else {
                            setListDataOffline(position);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0 && position < prikazaniKvizovi.size()) {
                        trenutniKviz = prikazaniKvizovi.get(position);
                        podaciOIgrama = new ArrayList<>();
                        pitanjaUKvizu = trenutniKviz.getPitanja();

                        Map<String, String> mapa = new KalendarResolver(KvizoviAkt.this).uzmiPodatke();
                        Date trenutniDatum = new Date();
                        String pattern = "yyyy-MM-dd";
                        SimpleDateFormat format = new SimpleDateFormat(pattern);
                        for (Map.Entry<String, String> ulaz : mapa.entrySet()) {
                            long vrijeme = Long.parseLong(ulaz.getKey());
                            Date datum = new Date(vrijeme);
                            if (format.format(trenutniDatum).equals(format.format(datum))) { // na danasnji datum
                                int trajanje = (int) Math.ceil(pitanjaUKvizu.size() / 2);
                                long miliTrajanje = TimeUnit.MINUTES.toMillis(trajanje);
                                long trenutnoVrijeme = trenutniDatum.getTime();
                                long miliEvent = datum.getTime();
                                int ostaloDoEVenta = (int) (TimeUnit.MILLISECONDS.toMinutes(miliEvent - trenutnoVrijeme) + 1);
                                if ((trenutnoVrijeme + miliTrajanje) > miliEvent && trenutnoVrijeme < miliEvent) {
                                    new AlertDialog.Builder(KvizoviAkt.this).setTitle("Greska").setMessage("Imate događaj u kalendaru za " + ostaloDoEVenta + " minuta!")
                                            .setNegativeButton("OK", null).show();
                                    return;
                                }
                            }
                        }
                        Intent intent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                        intent.putExtra("naziviSvihKategorija", naziviKategorija);
                        intent.putExtra("nazivKviza", trenutniKviz.getNaziv());
                        if (trenutniKviz.getKategorija() != null)
                            intent.putExtra("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
                        KvizoviAkt.this.startActivity(intent);
                    }

                }
            });

            lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position >= 0 && position < prikazaniKvizovi.size()) {
                            connected = spojenoNaMrezu();
                            Log.d("KONEKCIJA", String.valueOf(connected));
                            if (connected) {
                                trenutniKviz = prikazaniKvizovi.get(position);
                                pitanjaUKvizu = trenutniKviz.getPitanja();
                                Intent intent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                intent.putExtra("naziviSvihKategorija", naziviKategorija);
                                intent.putExtra("nazivKviza", trenutniKviz.getNaziv());
                                intent.putExtra("pitanjaUKvizu", pitanjaUKvizu);
                                if (trenutniKviz.getKategorija() != null)
                                    intent.putExtra("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
                                KvizoviAkt.this.startActivity(intent);
                            } else {
                                Toast.makeText(KvizoviAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
                            }
                            return true;
                        }

                    return false;
                }
            });

            footerView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    connected = spojenoNaMrezu();
                    Log.d("KONEKCIJA", String.valueOf(connected));
                    if (connected) {
                        trenutniKviz = null;
                        pitanjaUKvizu = new ArrayList<>();
                        Intent addQuizIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                        addQuizIntent.putExtra("naziviSvihKategorija", naziviKategorija);
                        addQuizIntent.putExtra("mogucaPitanja", pitanjaBaza);
                        KvizoviAkt.this.startActivity(addQuizIntent);
                    } else {
                        Toast.makeText(KvizoviAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
        }
        connected = spojenoNaMrezu();
        Log.d("KONEKCIJA", String.valueOf(connected));
        if (connected) {
            new UcitajPodatkeIzBazeTask(new AsyncResponse() {
                @Override
                public void processFinish(String output) {
                    prikazaniKvizovi.clear();
                    sviKvizovi.clear();
                    kategorije.clear();

                    prikazaniKvizovi.addAll(kvizoviBaza);
                    sviKvizovi.addAll(kvizoviBaza);
                    if (!siriEkran)
                        customAdapter.notifyDataSetChanged();

                    kategorije.addAll(kategorijeBaza);
                    if (naziviKategorija == null) naziviKategorija = new ArrayList<>();
                    naziviKategorija.clear();
                    naziviKategorija.add("Sve kategorije");
                    if (kategorije != null && kategorije.size() != 0) {
                        for (Kategorija k : kategorije) {
                            if (k != null)
                                naziviKategorija.add(k.getNaziv());
                        }
                    }
                    for (Kviz k : kvizoviBaza) {
                        bazaHelper.dodajKviz(k);
                        Log.d("IZ APP", "proslo");
                        if (k.getPitanja() != null && k.getPitanja().size() != 0) {
                            for (int i = 0; i < k.getPitanja().size(); i++) {
                                bazaHelper.dodajPitanje(k.getPitanja().get(i));
                                for (int j = 0; j < k.getPitanja().get(i).getOdgovori().size(); j++) {
                                    bazaHelper.dodajOdgovor(k.getPitanja().get(i).getOdgovori().get(j));
                                }
                            }
                        }
                    }
                    for (Kategorija k : kategorijeBaza) {
                        bazaHelper.dodajKategoriju(k);
                    }
                    if (!siriEkran)
                        adapter.notifyDataSetChanged();
                    if (siriEkran) {
                        FragmentManager fm = getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ListaFrag lf = new ListaFrag();
                        DetailFrag df = new DetailFrag();
                        Bundle bundle = new Bundle();
                        bundle.putStringArrayList("naziviSvihKategorija", naziviKategorija);
                        if (trenutniKviz != null && trenutniKviz.getKategorija() != null) {
                            bundle.putString("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
                            bundle.putString("nazivKviza", trenutniKviz.getNaziv());
                        }
                        df.setArguments(bundle);
                        ft.replace(R.id.listPlace, lf);
                        ft.replace(R.id.detailPlace, df).commit();
                    }
                }
            }, this).execute(" ");
        } else {
            // ucitaj podatke iz sqlite baze
            sviKvizovi = bazaHelper.ucitajKvizove();
            prikazaniKvizovi = bazaHelper.ucitajKvizove();
            kategorije = bazaHelper.ucitajKategorije();
            if (naziviKategorija == null) naziviKategorija = new ArrayList<>();
            naziviKategorija.clear();
            naziviKategorija.add("Sve kategorije");
            if (kategorije != null && kategorije.size() != 0) {
                for (Kategorija k : kategorije) {
                    if (k != null)
                        naziviKategorija.add(k.getNaziv());
                }
            }
            Toast.makeText(KvizoviAkt.this, "Nema internet konekcije!", Toast.LENGTH_SHORT).show();
        }

    }


    public boolean spojenoNaMrezu() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
            cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
            return true;
        return false;
    }

    /*public static boolean pristupInternetu() {
        try {
            String command = "ping -c 1 google.com";
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (Exception e) {
            return false;
        }
    }*/

    private void setListData (final String nazivKategorije) {
        new FiltrirajKvizoveTask(new AsyncResponse() {
            @Override
            public void processFinish(String output) {
                prikazaniKvizovi.clear();
                if (nazivKategorije.equals("Sve kategorije")) {
                    prikazaniKvizovi.addAll(kvizoviBaza);
                } else {
                    prikazaniKvizovi.addAll(filtriraniKvizovi);
                }
                customAdapter.notifyDataSetChanged();
            }
        }, this).execute(nazivKategorije);

    }

    public void setListDataOffline (int idKategorije) {
        if (idKategorije == 0) {
            prikazaniKvizovi.clear();
            for (Kviz k : sviKvizovi) {
                prikazaniKvizovi.add(k);
            }
        } else {
            prikazaniKvizovi.clear();
            String pomocna = naziviKategorija.get(idKategorije);
            for (Kviz k : sviKvizovi) {
                if (k.getKategorija() == null && pomocna.equals("Sve kategorije")) {
                    prikazaniKvizovi.add(k);
                } else if (k.getKategorija() != null && pomocna.equals(k.getKategorija().getNaziv())) {
                    prikazaniKvizovi.add(k);
                }
            }
        }
        customAdapter = new CustomAdapter(this, prikazaniKvizovi);
        lista.setAdapter(customAdapter);
    }

    @Override
    public void onItemClicked (final int pos) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        DetailFrag df = new DetailFrag();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("naziviSvihKategorija", naziviKategorija);
        if (trenutniKviz != null && trenutniKviz.getKategorija() != null) {
            bundle.putString("nazivKategorije", trenutniKviz.getKategorija().getNaziv());
            bundle.putString("nazivKviza", trenutniKviz.getNaziv());
        }
        df.setArguments(bundle);

        ft.replace(R.id.detailPlace, df).commit();

    }
}


