package ba.unsa.etf.rma.helper;

import static ba.unsa.etf.rma.helper.KategorijaDBOpenHelper.DATABASE_TABLE_KATEGORIJE;
import static ba.unsa.etf.rma.helper.KategorijaDBOpenHelper.KATEGORIJA_ID;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.DATABASE_TABLE_PITANJA;
import static ba.unsa.etf.rma.helper.PitanjeDBOpenHelper.PITANJE_ID;

public class KvizDBOpenHelper {
    public static final String DATABASE_TABLE_KVIZOVI = "kvizovi";
    public static final String KVIZ_ID = "_id";
    public static final String KVIZ_NAZIV = "nazivKviza";
    public static final String KVIZ_ID_KATEGORIJE = "idKategorije";
    public static final String DATABASE_TABLE_KVIZ_PITANJE = "kvizPitanje";
    public static final String KVIZ_PITANJE_IDKVIZA = "IdKviza";
    public static final String KVIZ_PITANJE_IDPITANJA = "idPitanja";


    public static final String CREATE_KVIZOVI = "CREATE TABLE " + DATABASE_TABLE_KVIZOVI +
            " ( " + KVIZ_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KVIZ_NAZIV + " TEXT, " +
            KVIZ_ID_KATEGORIJE + " INTEGER, " +
            "CONSTRAINT constr_tab_kvizovi_idkategorije FOREIGN KEY ( " + KVIZ_ID_KATEGORIJE + ") REFERENCES " + DATABASE_TABLE_KATEGORIJE + " ( " + KATEGORIJA_ID + " ) );";


    public static final String CREATE_KVIZOVI_PITANJA ="CREATE TABLE " + DATABASE_TABLE_KVIZ_PITANJE +
            " ( " + KVIZ_PITANJE_IDKVIZA + " TEXT NOT NULL, " +
            KVIZ_PITANJE_IDPITANJA + " TEXT NOT NULL, " +
            "CONSTRAINT constr_tab_idKviza FOREIGN KEY ( " + KVIZ_PITANJE_IDKVIZA + ") REFERENCES " + DATABASE_TABLE_KVIZOVI + " ( " + KVIZ_ID + " ), "+
            "CONSTRAINT constr_tab_idPitanja FOREIGN KEY ( " + KVIZ_PITANJE_IDPITANJA + ") REFERENCES " + DATABASE_TABLE_PITANJA + " ( " + PITANJE_ID + " ) "
            +");";


}
