package ba.unsa.etf.rma.fragmenti;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.fragmenti.InformacijeFrag.brojTacnihOdg;
import static ba.unsa.etf.rma.fragmenti.InformacijeFrag.brojPreostalihOdg;

public class PitanjeFrag extends Fragment {
    TextView nazivPitanja;
    ListView odgovoriNaPitanje;
    onItemClick oic;
    public static Pitanje prikazanoPitanje = null;
    public static int brojacPitanja = 0;
    ArrayList<String> naziviSvihKategorija;
    String nazivKviza = "";
    String nazivKategorije = "";
    public static ArrayList<Pitanje> pitanjaaa = null;

    public interface onItemClick {
        void onItemClicked(int pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        nazivKviza = "";
        //pitanjaaa = trenutniKviz.dajRandomPitanja();
        View iv = inflater.inflate(R.layout.fragment_pitanje, container, false);
        nazivPitanja = iv.findViewById(R.id.tekstPitanja);
        odgovoriNaPitanje = iv.findViewById(R.id.odgovoriPitanja);
        return iv;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayList<String> odgovori = new ArrayList<>();

        naziviSvihKategorija = getActivity().getIntent().getStringArrayListExtra("naziviSvihKategorija");
        nazivKviza = getActivity().getIntent().getStringExtra("nazivKviza");
        nazivKategorije = getActivity().getIntent().getStringExtra("nazivKategorije");

        if (pitanjaaa != null && brojacPitanja == pitanjaaa.size()) { // nema vise pitanja u kvizu
            nazivPitanja.setText("Kviz je zavrsen!");
            brojacPitanja = 0;

        } else if (pitanjaaa != null){
            if (getArguments() != null && getArguments().containsKey("pozicija")) {
                int poz = getArguments().getInt("pozicija");
                if (brojacPitanja >= 0 && brojacPitanja < pitanjaaa.size()) {
                    prikazanoPitanje = pitanjaaa.get(brojacPitanja);
                    nazivPitanja.setText(prikazanoPitanje.getNaziv());
                }
            } else if (pitanjaaa != null) {
                prikazanoPitanje = pitanjaaa.get(brojacPitanja);
                nazivPitanja.setText(prikazanoPitanje.getNaziv());
            }
            if (prikazanoPitanje != null) {
                odgovori = prikazanoPitanje.dajRandomOdgovore();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, odgovori);
                odgovoriNaPitanje.setAdapter(adapter);
            }
        } else { // ako nema pitanja
            nazivPitanja.setText("Kviz je zavrsen!");
            brojacPitanja = 0;
        }
        try {
            oic = (onItemClick) getActivity();
        } catch (ClassCastException e) {

        }
        final ArrayList<String> finalOdgovori = odgovori;
        final int[] pozicijaTacnog = new int[1];
        pozicijaTacnog[0] = -1; // da po defaultu bude bez boje
        final int[] pozicijaNetacnog = new int[1];
        pozicijaNetacnog[0] = -1;

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, finalOdgovori) {
            @Override
            public View getView (final int position, View convertView, ViewGroup parent){
                View v = super.getView(position, convertView, parent);
                if (position == pozicijaTacnog[0]) {
                    v.setBackgroundColor(getResources().getColor(R.color.zelena));
                } else if (position == pozicijaNetacnog[0]) {
                    v.setBackgroundColor(getResources().getColor(R.color.crvena));
                } else {
                    v.setBackgroundColor(Color.TRANSPARENT);
                }

                return v;
            }
        };
        odgovoriNaPitanje.setAdapter(adapter);

        odgovoriNaPitanje.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (prikazanoPitanje.getTacan().equals(finalOdgovori.get(position))) {
                    pozicijaTacnog[0] = position;
                    brojTacnihOdg++;
                } else {
                    pozicijaNetacnog[0] = position;
                }
                pozicijaTacnog[0] = adapter.getPosition(prikazanoPitanje.getTacan());
                brojPreostalihOdg--;
                odgovoriNaPitanje.setAdapter(adapter);
                oic.onItemClicked(position);
            }
        });

    }

}
