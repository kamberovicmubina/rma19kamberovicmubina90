package ba.unsa.etf.rma.helper;

import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.DATABASE_TABLE_KVIZOVI;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_ID;
import static ba.unsa.etf.rma.helper.KvizDBOpenHelper.KVIZ_NAZIV;

public class RanglistaDBOpenHelper  {
    public static final String DATABASE_TABLE_RANGLISTE = "rangliste";
    public static final String RANGLISTA_ID = "_id";
    public static final String RANGLISTA_NAZIV_KVIZA = "nazivKviza";
    public static final String RANGLISTA_REZULTAT = "rezultatKviza";
    public static final String RANGLISTA_IGRAC = "igracKviza";

    public static final String CREATE_RANGLISTE = "CREATE TABLE " + DATABASE_TABLE_RANGLISTE +
            " ( " + RANGLISTA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            RANGLISTA_NAZIV_KVIZA + " TEXT NOT NULL, "+
            RANGLISTA_REZULTAT + " REAL NOT NULL, "+
            RANGLISTA_IGRAC + " TEXT NOT NULL, " +
            " CONSTRAINT constr_tab_ranglista_idkviza FOREIGN KEY ( " + RANGLISTA_NAZIV_KVIZA + " ) REFERENCES " + DATABASE_TABLE_KVIZOVI + "(" + KVIZ_NAZIV + "), "+
            " CONSTRAINT constr_tab_ranglista_rezultat CHECK ( " + RANGLISTA_REZULTAT + " BETWEEN 0 AND 1) );";
}
