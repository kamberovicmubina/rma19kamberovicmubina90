package ba.unsa.etf.rma.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.klase.PodService;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.fragmenti.InformacijeFrag.procenatTacnihOdg;
import static ba.unsa.etf.rma.fragmenti.RangLista.podaciOIgrama;

public class DodajRanglistuTask extends AsyncTask<String, Void, String> {
    Context context;

    public DodajRanglistuTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {

        InputStream is = context.getResources().openRawResource(R.raw.secret);
        GoogleCredential credentials = null;
        String TOKEN = "";
        try {
            credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
            credentials.refreshToken();
            TOKEN = credentials.getAccessToken();
            Log.d("TOKEN DODAVANJE", TOKEN);
            String url = "https://firestore.googleapis.com/v1/projects/kamberovicmubinarma90-240912/databases/(default)/documents/Rangliste?documentId=" + trenutniKviz.getId() + "&access_token=";
            HttpURLConnection conn = null;
            try {
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String dokument = "{ \"fields\": { \"nazivKviza\": {\"stringValue\":\"" + trenutniKviz.getNaziv() + "\"}, " +
                    "\"lista\": {\"mapValue\": {\"fields\": {\"1\": {\"mapValue\": {\"fields\": {\"" + strings[0] + "\": {\"doubleValue\": " + procenatTacnihOdg + "}}}}";

            dokument += "}}}}}";

            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = dokument.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }
            int code = conn.getResponseCode();
            InputStream odgovor = conn.getInputStream();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(odgovor, StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                Log.d("NOVa RAng lista", response.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}


