package ba.unsa.etf.rma.aktivnosti;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.provider.AlarmClock;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.net.URI;
import java.util.ArrayList;

import ba.unsa.etf.rma.AsyncResponse;
import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.helper.BazaHelper;
import ba.unsa.etf.rma.task.DodajRanglistuTask;
import ba.unsa.etf.rma.task.DodajURanglistuTask;
import ba.unsa.etf.rma.task.UcitajRanglistuTask;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.trenutniKviz;
import static ba.unsa.etf.rma.fragmenti.PitanjeFrag.brojacPitanja;
import static ba.unsa.etf.rma.fragmenti.PitanjeFrag.pitanjaaa;
import static ba.unsa.etf.rma.fragmenti.InformacijeFrag.brojTacnihOdg;
import static ba.unsa.etf.rma.fragmenti.InformacijeFrag.brojPreostalihOdg;
import static ba.unsa.etf.rma.fragmenti.RangLista.podaciOIgrama;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.connected;


public class IgrajKvizAkt extends AppCompatActivity implements PitanjeFrag.onItemClick {
    String nazivKviza;
    ArrayList<String> naziviSvihKategorija;
    String nazivKategorije = "";
    String imeIgraca = "";
    Uri timerData;
    BazaHelper bazaHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igraj_kviz_akt);
        bazaHelper = new BazaHelper(IgrajKvizAkt.this);

        pitanjaaa = trenutniKviz.dajRandomPitanja();
        brojacPitanja = 0; // ovo je dodano

        nazivKviza = getIntent().getStringExtra("nazivKviza");
        naziviSvihKategorija = getIntent().getStringArrayListExtra("naziviSvihKategorija");
        nazivKategorije = getIntent().getStringExtra("nazivKategorije");

        // aktiviranje alarma
        if (trenutniKviz.getPitanja() != null && trenutniKviz.getPitanja().size() != 0) {
            Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER);
            int minute = (int) Math.ceil((double) trenutniKviz.getPitanja().size() / 2);
            Log.d("MINUTE", String.valueOf(minute));
            if (trenutniKviz.getPitanja().size() == 1) // kviz ima jedno pitanje
                minute = 60; // pretvaramo u sekunde
            else minute *= 60;
            Log.d("SEKUNDE", String.valueOf(minute));
            intent.putExtra(AlarmClock.EXTRA_LENGTH, minute);
            intent.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
            timerData = intent.getData();
            this.startActivity(intent);

        }

       FragmentManager fm = getSupportFragmentManager();
       FragmentTransaction ft = fm.beginTransaction();

       InformacijeFrag fragmentInfo = new InformacijeFrag();
       PitanjeFrag fragmentPitanje = new PitanjeFrag();

       ft.replace(R.id.informacijePlace, fragmentInfo);
       ft.replace(R.id.pitanjePlace, fragmentPitanje);

        Bundle argumenti = new Bundle();
        argumenti.putString("nazivKviza", nazivKviza);
        argumenti.putStringArrayList("naziviSvihKategorija", naziviSvihKategorija);
        argumenti.putString("nazivKategorije", nazivKategorije);
        fragmentInfo.setArguments(argumenti);
        fragmentPitanje.setArguments(argumenti);
        ft.commit();


    }

    @Override
    public void onItemClicked (final int pos) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // nakon dvije sekunde radi sljedece
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                PitanjeFrag pFrag = new PitanjeFrag();
                Bundle bundle = new Bundle();
                bundle.putInt("pozicija", pos);
                brojacPitanja++;
                pFrag.setArguments(bundle);

                InformacijeFrag iFrag = new InformacijeFrag();
                Bundle bundleInfo = new Bundle();
                bundleInfo.putInt("pritisnutiOdgovor", pos);
                iFrag.setArguments(bundleInfo);

                ft.replace(R.id.pitanjePlace, pFrag);
                ft.replace(R.id.informacijePlace, iFrag).commit();
                if (brojPreostalihOdg == 0) {
                    /*Intent intent = new Intent(AlarmClock.ACTION_DISMISS_TIMER);
                    intent.setData(timerData);
                    getApplicationContext().startActivity(intent);*/

                    unesiImeZaRangListu(); // zavrsen kviz
                }
            }
        }, 2000);

    }

    public void unesiImeZaRangListu () {
        LayoutInflater li = LayoutInflater.from(IgrajKvizAkt.this);
        View unosView = li.inflate(R.layout.unos_imena, null);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(IgrajKvizAkt.this).setView(unosView);
        final EditText ime = unosView.findViewById(R.id.unosText);

        alertDialog.setMessage("Unesite svoje ime: ").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                imeIgraca = String.valueOf(ime.getText());
                connected = spojenoNaMrezu();
                Log.d("KONEKCIJA", String.valueOf(connected));
                if (connected) {
                    new UcitajRanglistuTask(IgrajKvizAkt.this, new AsyncResponse() {
                        @Override
                        public void processFinish(String output) {
                            if (podaciOIgrama != null && podaciOIgrama.size() != 0)
                                new DodajURanglistuTask(IgrajKvizAkt.this).execute(String.valueOf(ime.getText()));
                            else
                                new DodajRanglistuTask(IgrajKvizAkt.this).execute(String.valueOf(ime.getText()));
                            new UcitajRanglistuTask(IgrajKvizAkt.this, new AsyncResponse() {
                                @Override
                                public void processFinish(String output) {
                                    FragmentManager fm = getSupportFragmentManager();
                                    FragmentTransaction ft = fm.beginTransaction();
                                    RangLista rangLista = new RangLista();
                                    ft.replace(R.id.pitanjePlace, rangLista).commit();
                                }
                            }).execute(" ");
                        }
                    }).execute(" ");
                } else {
                    // ubacuje ranglistu u sqlite bazu
                    if (trenutniKviz.getPitanja().size() != 0) { // ako se uopce igrao kviz
                        double rezultat = brojTacnihOdg / trenutniKviz.getPitanja().size();
                        bazaHelper.dodajURanglistu(trenutniKviz.getNaziv(), String.valueOf(ime.getText()), rezultat);
                        String s;
                        if (podaciOIgrama != null && podaciOIgrama.size() != 0)
                            s = podaciOIgrama.size() + 1 + " ";
                        else s = "1. ";
                        s = s + ime.getText() + ": " + String.valueOf(rezultat) + "%";
                        podaciOIgrama.add(s); // podsjetiti se kako izgleda string
                        bazaHelper.ucitajRangliste();
                        FragmentManager fm = getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        RangLista rangLista = new RangLista();
                        ft.replace(R.id.pitanjePlace, rangLista).commit();
                    }
                }
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
        });
        AlertDialog al = alertDialog.create();
        al.show();
    }

    public boolean spojenoNaMrezu() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
            return true;
        return false;
    }

    @Override
    public void onBackPressed () {
        brojacPitanja = 0;
        brojTacnihOdg = 0;
        brojPreostalihOdg = 0;
        pitanjaaa = trenutniKviz.dajRandomPitanja();
        Intent returnIntent = new Intent(getApplicationContext(), KvizoviAkt.class);
        returnIntent.putExtra("naziviSvihKategorija", naziviSvihKategorija);
        startActivity(returnIntent);
    }

}
